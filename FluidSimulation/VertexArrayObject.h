// Copyright (c) 2021 Mugurel-Adrian Enache

#pragma once
#include <memory>
#include "IndexBufferObject.h"
#include "VertexBufferObject.h"

/**
 * @brief VertexArrayObject
 *
 * Responsible for composing a VAO.
 * @tparam T
 * @tparam V
*/
template <class T, class V>
class VertexArrayObject
{
public:
	uint32_t id = 0;
	std::shared_ptr<VertexBufferObject<T>> vbo;
	std::shared_ptr<IndexBufferObject<V>> ibo;

public:
	VertexArrayObject<T, V>(std::shared_ptr<VertexBufferObject<T>> vbo, std::shared_ptr<IndexBufferObject<V>> ibo);
	void generate();
	void bind();
	void unbind();
	void draw(GLenum mode = GL_TRIANGLES);
};

template<class T, class V>
inline VertexArrayObject<T, V>::VertexArrayObject(std::shared_ptr<VertexBufferObject<T>> vbo, std::shared_ptr<IndexBufferObject<V>> ibo)
{
	this->vbo = vbo;
	this->ibo = ibo;
}

template<class T, class V>
inline void VertexArrayObject<T, V>::generate()
{
	glGenVertexArrays(1, &id);
}

template<class T, class V>
inline void VertexArrayObject<T, V>::bind()
{
	glBindVertexArray(id);
}

template<class T, class V>
inline void VertexArrayObject<T, V>::unbind()
{
	glBindVertexArray(0);
}

template<class T, class V>
inline void VertexArrayObject<T, V>::draw(GLenum mode)
{
	if (vbo && ibo)
	{
		ibo->draw(mode);
	}
	else if(vbo)
	{
		vbo->draw(mode);
	}
}
