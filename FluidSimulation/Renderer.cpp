// Copyright (c) 2020 Mugurel-Adrian Enache

#include "Renderer.h"
#include "MainScene.h"
#include "Utility.h"

/**
* @brief Initializes GLFW and creates a GLFW Window, and the globals wrapper
*/
Renderer::Renderer()
{
	globals = std::make_shared<GlobalsWrapper>();

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	if (!glfwInit())
	{
		std::cerr << "Error: Could not initialize glfw." << std::endl;
	}

	// Window Creation
	window = std::make_shared<GLFWwindow*>(glfwCreateWindow(globals->width, globals->height, "Fluid Simulator", nullptr, nullptr));
	assert(window.get() && "Could not create glfw window.");

	// Sets the context and swap interval
	glfwMakeContextCurrent(*window.get());
	glfwSwapInterval(1);

	bool glinit = (gl3wInit() != 0);
	assert(!glinit && "Could not initialize gl3w.");

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

/**
 * @brief Destroys the window and glfw.
 */
Renderer::~Renderer()
{
	glfwDestroyWindow(*window.get());
	glfwTerminate();
}

/**
 * @brief Initializes the UI Handler
 *
 * Initializes OpenGL3 within the window
 *
 * Creates an UI Scene and references the color texture
 * that will be rendered offscreen
 */
void Renderer::initUI()
{
	userInterface = std::make_unique<UIHandler>();
	userInterface->initWindowAndGL3(window, "#version 450");
	userInterface->uiScenes.emplace_back(std::make_shared<MainScene>(globals->width, globals->height, colorTexture));
	auto scene = std::dynamic_pointer_cast<MainScene>(userInterface->uiScenes[0]);
	scene->globals = globals;
}

/**
 * @brief Generates the frame-buffers and textures
 *
 */
void Renderer::generalInit()
{
	glfwGetFramebufferSize(*window.get(), &globals->renderWidth, &globals->renderHeight);

	glEnable(GL_DEPTH_TEST);

	// Generating the framebuffer and binding it
	glGenFramebuffers(1, &renderFramebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, renderFramebuffer);

	// Generating the texture we want to render color to
	glGenTextures(1, &colorTexture);
	glBindTexture(GL_TEXTURE_2D, colorTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, globals->renderWidth, globals->renderHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// Attach color texture
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, colorTexture, 0);

	// Generate the depth renderbuffer
	GLuint depthRenderbuffer;
	glGenRenderbuffers(1, &depthRenderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, globals->renderWidth, globals->renderHeight);

	// Attach the render-buffer
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);

	// Unbinding textures and framebuffers
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	assert(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);

	// Creating the shader programs
	colorShaderProgram = std::make_unique<ShaderModule>("color_4.vert", "color_4.frag");
	gridShaderProgram = std::make_unique<ShaderModule>("color.vert", "color.frag");
	lightShaderProgram = std::make_unique<ShaderModule>("lighting_shader.vert", "texlighting_shader.frag");

	globals->generalInit();
}

/**
 * @brief Simply renders every object in the screen as needed
 */
void Renderer::renderLoop()
{
	auto initTime = std::chrono::high_resolution_clock::now();
	auto incTime = std::chrono::high_resolution_clock::now();

	float fps = 0.0f;
	float dt = 0.00f;
	while (!glfwWindowShouldClose(*window.get()))
	{
		auto frameTimeBegin = std::chrono::high_resolution_clock::now();

		glfwPollEvents();

		userInterface->nextFrame();

		// Normal rendering
		{
			glBindFramebuffer(GL_FRAMEBUFFER, renderFramebuffer);
			glViewport(0, 0, globals->renderWidth, globals->renderHeight);
			glClearColor(clearColor.x, clearColor.y, clearColor.z, clearColor.w);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			lightShaderProgram->use();

			glUniformMatrix4fv(glGetUniformLocation(lightShaderProgram->id, "view"), 1, GL_FALSE, (&globals->camera->transform.matrix[0][0]));
			glUniformMatrix4fv(glGetUniformLocation(lightShaderProgram->id, "projection"), 1, GL_FALSE, (&globals->camera->projection[0][0]));

			glUniform4fv(glGetUniformLocation(lightShaderProgram->id, "cameraPos"), 1, &globals->camera->transform.position[0]);

			// Load light props
			if (globals->lightObject)
			{
				globals->lightObject->draw(lightShaderProgram->id);
			}

			if (globals->fluidObject)
			{
				if (globals->shouldSimulate)
				{
					globals->fluidObject->simulate(globals->fluidObject->stepSize);
				}

				globals->fluidObject->draw(GL_TRIANGLES, lightShaderProgram->id);
			}

			if (globals->containerObject)
			{
				glDisable(GL_CULL_FACE);
				globals->containerObject->draw(GL_TRIANGLES, lightShaderProgram->id);
				glEnable(GL_CULL_FACE);
			}


			lightShaderProgram->unbind();


			if (globals->mousePosX * globals->mousePosY >= 0)
			{
				glReadPixels(static_cast<int>(globals->mousePosX), static_cast<int>(globals->mousePosY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &globals->mouseDepthValue);
				//std::cout << "Depth value: " << globals->mouseDepthValue << std::endl;
			}

			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		}

		// Renders the UI
		userInterface->render();
		glfwSwapBuffers(*window.get());

		auto frameTimeEnd = std::chrono::high_resolution_clock::now();
		auto frameDuration = std::chrono::duration_cast<std::chrono::milliseconds>(frameTimeEnd - frameTimeBegin);

		dt = static_cast<float>(frameDuration.count()) / 1000.f;

		auto fullDuration = std::chrono::duration_cast<std::chrono::milliseconds>(incTime - initTime);

		if (fullDuration.count() < 1000)
		{
			fps++;
			incTime += frameDuration;
		}
		else
		{
			fps = 0.0f;
			initTime = incTime;
		}
	}
}