// Copyright (c) 2020 Mugurel-Adrian Enache

#version 450

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 normal;
layout(location = 2) in vec4 color;
layout(location = 3) in vec2 uv;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

layout(location = 0) out vec4 fragPos;
layout(location = 1) out vec4 fragNormal;
layout(location = 2) out vec4 fragColor;
layout(location = 3) out vec2 fragUV;

void main()
{
	fragPos = model * position;
	fragNormal = transpose(inverse(model)) * normalize(normal);
	fragColor = color;
	fragUV = uv;

	gl_Position = projection * view * fragPos;
}