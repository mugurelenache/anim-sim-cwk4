// Copyright (c) 2021 Mugurel-Adrian Enache

#pragma once
#include <vector>
#include <glm/glm.hpp>
#include <memory>
#include "Vertex.h"
#include "BoundingBox.h"
#include "VertexBufferObject.h"
#include "IndexBufferObject.h"
#include "VertexArrayObject.h"

/**
 * @brief Mesh
 *
 * Responsible for keeping VBO, IBO, VAOs.
 * Stores a bounding box as well.
*/
class Mesh
{
public:
	std::shared_ptr<VertexBufferObject<Vertex>> vertexBuffer;
	std::shared_ptr<IndexBufferObject<uint32_t>> indexBuffer;
	std::unique_ptr<VertexArrayObject<Vertex, uint32_t>> vao;

public:
	// Constructor
	Mesh(std::shared_ptr<VertexBufferObject<Vertex>> vertexBuffer, std::shared_ptr<IndexBufferObject<uint32_t>> indexBuffer);
	void generateVAO(GLenum usage = GL_STATIC_DRAW);

public:
	static VertexBufferObject<Vertex> generateDefaultTriangleVertices();
	static IndexBufferObject<uint32_t> generateDefaultTriangleIndices();
	static VertexBufferObject<Vertex> generateDefaultQuadVertices();
	static IndexBufferObject<uint32_t> generateDefaultQuadIndices();
};