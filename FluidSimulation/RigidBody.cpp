// Copyright (c) 2021 Mugurel-Adrian Enache

#include "RigidBody.h"
#include "Object.h"
#include <iostream>
#include "GlobalsWrapper.h"
#include "Utility.h"
#include "ObjectParticle.h"

/**
 * @brief Creates a rigid body for physics simulation
 * @param velocity
 * @param acceleration
 * @param force
 * @param mass
 * @param parent
*/
RigidBody::RigidBody(const glm::vec4& velocity, const glm::vec4& acceleration, const glm::vec4& force, const float& mass, Object* parent) : Component(parent)
{
	this->velocity = velocity;
	this->acceleration = acceleration;
	this->force = force;
	this->mass = mass;
}

RigidBody::RigidBody(const RigidBody& other) : Component(other)
{
	this->velocity = other.velocity;
	this->acceleration = other.acceleration;
	this->force = other.force;
	this->mass = other.mass;

	this->hasCollided = other.hasCollided;
	this->isClose = other.isClose;
	this->gravity = other.gravity;
}

/**
 * @brief Destructor, does not destroy anything
*/
RigidBody::~RigidBody()
{
	// Does not destroy anything
}

/**
 * @brief Simulates explicit euler
 * @param timeStep
 * @param externalForce
*/
void RigidBody::simulateExplicitEuler(const float& timeStep, const glm::vec4& externalForce)
{
	if (parent)
	{
		//glm::vec4 airResistance = -velocity * parent->globals->simulatedObject->airResistanceCoefficient;
		glm::vec4 airResistance = -velocity * 0.001f;
		parent->transform.setPosition(parent->transform.position + timeStep * glm::vec3(velocity));
		force = (mass * gravity + externalForce + airResistance);
		velocity = velocity + timeStep * acceleration;
		acceleration = force / mass;
	}
	else
	{
		assert(false && "Error: Cannot simulate rigid body as it has no parent");
	}
}

/**
 * @brief Simulates semi-explicit euler
 * @param timeStep
 * @param externalForce
*/
void RigidBody::simulateSemiExplicitEuler(const float& timeStep, const glm::vec4& externalForce)
{
	if (parent)
	{
		//glm::vec4 airResistance = -velocity * parent->globals->simulatedObject->airResistanceCoefficient;
		glm::vec4 airResistance = -velocity * 0.001f;
		velocity = velocity + timeStep * acceleration;
		parent->transform.setPosition(parent->transform.position + timeStep * glm::vec3(velocity));
		force = (mass * gravity + externalForce + airResistance);
		acceleration = force / mass;
	}
	else
	{
		assert(false && "Error: Cannot simulate rigid body as it has no parent");
	}
}

/**
 * @brief Main simulation function (called by other objects)
 * The others are unaware of what simulation function is behind the-scenes
 * @param timeStep
 * @param externalForce
*/
void RigidBody::simulate(const float& timeStep, const glm::vec4& externalForce)
{
	//simulateSemiExplicitEuler(timeStep, externalForce);
	//simulateVerlet(timeStep, externalForce);
	simulateLeapfrog(timeStep, externalForce);

	dynamic_cast<ObjectParticle*>(parent)->collider->center = glm::vec4(parent->transform.position.x, parent->transform.position.y, parent->transform.position.z, 1.0f);
	checkCollision();
}

/**
 * @brief Simulates a trial-and-error iterative stepping algorithm that is a little bit more resistant to numerical errors (somehow!)
 * @param timeStep
 * @param externalForce
*/
void RigidBody::simulateTrial(const float& timeStep, const glm::vec4& externalForce)
{
	if (parent)
	{
		//glm::vec4 airResistance = -velocity * parent->globals->simulatedObject->airResistanceCoefficient;
		glm::vec4 airResistance = -velocity * 0.001f;
		force = (mass * gravity + externalForce + airResistance);
		acceleration = force / mass;

		velocity = velocity + timeStep * acceleration;
		parent->transform.setPosition(parent->transform.position + timeStep * glm::vec3(velocity));
	}
	else
	{
		assert(false && "Error: Cannot simulate rigid body as it has no parent");
	}
}

/**
 * @brief Simulates verlet
 * @param timeStep
 * @param externalForce
*/
void RigidBody::simulateVerlet(const float& timeStep, const glm::vec4& externalForce)
{
	if (parent)
	{
		//glm::vec4 airResistance = -velocity * parent->globals->simulatedObject->airResistanceCoefficient;
		glm::vec4 airResistance = -velocity * 0.001f;

		auto rstep = timeStep * velocity + 0.5f * acceleration * timeStep * timeStep;
		parent->transform.setPosition(parent->transform.position + glm::vec3(rstep));
		auto hVelocity = velocity + 0.5f * acceleration * timeStep;

		force = (mass * gravity + externalForce + airResistance);
		acceleration = force / mass;

		velocity = hVelocity + 0.5f * acceleration * timeStep;

	}
	else
	{
		assert(false && "Error: Cannot simulate rigid body as it has no parent");
	}
}

/**
 * @brief Simulates the leapfrog scheme
 * @param timeStep
 * @param externalForce
*/
void RigidBody::simulateLeapfrog(const float& timeStep, const glm::vec4& externalForce)
{
	if (parent)
	{
		auto halfVelocity = velocity + acceleration * 0.5f * timeStep;

		parent->transform.setPosition(parent->transform.position + glm::vec3(halfVelocity) * timeStep);

		ObjectParticle* particle = dynamic_cast<ObjectParticle*>(parent);

		if (particle)
		{
			force = particle->density * gravity + externalForce;
			acceleration = force / particle->density;
		}
		else
		{
			force = mass * gravity + externalForce;
			acceleration = force / mass;
		}

		velocity = halfVelocity + acceleration * 0.5f * timeStep;
	}
	else
	{
		assert(false && "Error: Cannot simulate rigid body as it has no parent");
	}
}

/**
 * @brief Checks the collision between the rigid body and all the scene objects
 * Saves the id of the other scene collider id for later checks and usages
*/
void RigidBody::checkCollision()
{
	if (parent)
	{
		if (parent->globals)
		{
			hasCollided = false;

			// Need to collide only with the container
			float eps = 0.001f;

			float radius = dynamic_cast<ObjectParticle*>(parent)->collider->radius;

			// CONTAINER
			if (parent->transform.position.y < parent->globals->containerObject->transform.position.y + parent->globals->containerObject->height)
			{
				if (parent->transform.position.x - radius < parent->globals->containerObject->transform.position.x - parent->globals->containerObject->width / 2.f)
				{
					// Left wall
					hasCollided = true;
					velocity = 0.5f * glm::reflect(velocity, glm::vec4(1.0f, 0.0f, 0.0f, 0.0f));
					parent->transform.setPosition(glm::vec3(parent->globals->containerObject->transform.position.x - parent->globals->containerObject->width / 2.f + eps + radius, parent->transform.position.y, parent->transform.position.z));
				}
				if (parent->transform.position.x + radius > parent->globals->containerObject->transform.position.x + parent->globals->containerObject->width / 2.f)
				{
					// Right wall
					hasCollided = true;
					velocity = 0.5f * glm::reflect(velocity, glm::vec4(-1.0f, 0.0f, 0.0f, 0.0f));
					parent->transform.setPosition(glm::vec3(parent->globals->containerObject->transform.position.x + parent->globals->containerObject->width / 2.f - eps - radius, parent->transform.position.y, parent->transform.position.z));
				}
				if (parent->transform.position.y - radius < parent->globals->containerObject->transform.position.y)
				{
					// Bottom wall
					hasCollided = true;
					velocity = 0.5f * glm::reflect(velocity, glm::vec4(0.0f, 1.0f, 0.0f, 0.0f));
					parent->transform.setPosition(glm::vec3(parent->transform.position.x, parent->globals->containerObject->transform.position.y + eps + radius, parent->transform.position.z));
				}
			}
			/* Particle-particle, but not working :D
			const auto& fluid = parent->globals->fluidObject;

			for (size_t i = 0; i < fluid->particles.size(); i++)
			{
				if(dynamic_cast<ObjectParticle*>(parent)->collider->isInside(fluid->particles[i].collider->center) && (this != (fluid->particles[i].rigidBody.get())))
				{
					glm::vec3 dir = -(fluid->particles[i].transform.position - parent->transform.position);

					velocity = 0.5f * -velocity * glm::normalize(glm::vec4(dir.x, dir.y, dir.z, 1.0f)) * eps;
					break;
				}
			}
			*/
		}
		else
		{
			if (parent->transform.position.y <= 0)
			{
				parent->transform.setPosition(glm::vec3(parent->transform.position.x, 0.0f, parent->transform.position.z));
				velocity = glm::vec4(0.0f);
			}
		}
	}
}

/**
 * @brief Checks if the object is close to the collider
*/
void RigidBody::checkClose()
{
	if (parent)
	{
		if (parent->globals)
		{
			isClose = false;
		}
	}
}
