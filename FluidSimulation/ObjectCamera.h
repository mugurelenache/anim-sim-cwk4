// Copyright (c) 2021 Mugurel-Adrian Enache

#pragma once
#include <glm/glm.hpp>
#include "Object.h"

/**
 * @brief ObjectCamera
 *
 * Derived class containing more data about an object that
 * implements a camera under the hood.
*/
class ObjectCamera : public Object
{
public:
	float nearPlane = 0.001f;
	float farPlane = 100.f;
	glm::mat4 projection = glm::mat4(1.0f);

	glm::vec3 forward = glm::vec3(0.0f, 0.0f, -1.0f);
	glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);

public:
	// Constructor
	ObjectCamera(const float& nearPlane = 0.0001f, const float& farPlane = 100.f, const glm::mat4& projection = glm::mat4(1.0f));
};