// Copyright (c) 2021 Mugurel-Adrian Enache

#version 450
uniform vec4 customColor;

out vec4 FragColor;

void main()
{
	FragColor = vec4(customColor);
}