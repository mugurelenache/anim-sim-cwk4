// Copyright (c) 2021 Mugurel-Adrian Enache

#include "UIHandler.h"
#include <GL/gl3w.h>
#include "MainScene.h"
#include <iostream>
#include <GLFW/glfw3.h>

/**
 * @brief Checks the version of imgui and launches an imgui context
*/
UIHandler::UIHandler()
{
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();

	ImGuiIO& io = ImGui::GetIO();
	ImGui::StyleColorsDark();
}

/**
 * @brief Stops the imgui opengl+glfw instance
 * and then it destroys the imgui context
*/
UIHandler::~UIHandler()
{
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();
}

/**
 * @param window -- ptr to a GLFWwindow*
 * @param glslVersion -- current supported glsl version
 * @brief Initializes the GLFW + GL imgui implementation
*/
void UIHandler::initWindowAndGL3(std::shared_ptr<GLFWwindow*> window, const std::string& glslVersion)
{
	ImGui_ImplGlfw_InitForOpenGL(*window.get(), true);
	ImGui_ImplOpenGL3_Init(glslVersion.c_str());

	this->window = window;
}

/**
 * @brief Calls the imgui next frame
*/
void UIHandler::nextFrame()
{
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();
}

/**
 * @brief Renders the UI for each active scene
*/
void UIHandler::render()
{
	for (const auto& scene : uiScenes)
	{
		scene->render();
	}

	ImGui::Render();

	int width;
	int height;

	// Renders everything in the main framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glfwGetFramebufferSize(*window.get(), &width, &height);
	glViewport(0, 0, width, height);
	glClearColor(0.f, 0.f, 0.f, 1.f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}