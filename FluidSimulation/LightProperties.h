// Copyright (c) 2021 Mugurel-Adrian Enache

#pragma once
#include "Component.h"
#include "glm/glm.hpp"

/**
 * @brief Light properties for colors etc
*/
class LightProperties : public Component
{
public:
	glm::vec4 ambient = glm::vec4(0.2f, 0.2f, 0.2f, 1.0f);
	glm::vec4 diffuse = glm::vec4(1.0f);
	glm::vec4 specular = glm::vec4(0.5f, 0.5f, 0.5f, 1.0f);
	float specularPower = 16.f;

public:
	LightProperties(const glm::vec4& diffuse = glm::vec4(1.0f), const glm::vec4& specular = glm::vec4(0.5f, 0.5f, 0.5f, 1.0f), const glm::vec4 ambient = glm::vec4(0.2f, 0.2f, 0.2f, 1.0f), float specularPower = 16.f, Object* object = nullptr);
};