// Copyright (c) 2021 Mugurel-Adrian Enache

#include "Component.h"

/**
 * @brief Component constructor, sets its parent
 * @param parent
*/
Component::Component(Object* parent)
{
	this->parent = parent;
}

/**
 * @brief Copy-constructor
 * @param other
*/
Component::Component(const Component& other)
{
	this->parent = other.parent;
}

/**
 * @brief Component destructor, does not destroy parent ptr
*/
Component::~Component()
{
	// Don't delete the parent pointer
}