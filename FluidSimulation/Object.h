// Copyright (c) 2021 Mugurel-Adrian Enache

#pragma once
#include <memory>
#include "Mesh.h"
#include "Transform.h"

// FWD Declaration
class GlobalsWrapper;
class Component;

/**
 * @brief Object
 *
 * Contains a base structure for an object.
 * It currently has a transform component.
*/
class Object
{
public:
	GlobalsWrapper* globals;
	Transform transform;

public:
	Object(GlobalsWrapper* globals = nullptr);
	Object(const Object& other);
	virtual ~Object();
};