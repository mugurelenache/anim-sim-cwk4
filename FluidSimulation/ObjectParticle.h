#pragma once
#include "Object.h"
#include <glm/glm.hpp>
#include <GL/gl3w.h>
#include "RigidBody.h"
#include "Material.h"
#include "Collider3D.h"
#include "BoundingSphere.h"

class ObjectParticle : public Object
{
public:
	std::shared_ptr<Mesh> mesh;
	std::shared_ptr<Material> material;

	std::unique_ptr<RigidBody> rigidBody;
	std::unique_ptr<BoundingSphere> collider;

	float pressure = 0.0f;
	float initialDensity = 0.0f;
	float density = 0.0f;

public:
	ObjectParticle(std::shared_ptr<Mesh> mesh, std::shared_ptr<Material> material, const float& radius = 0.0f, GlobalsWrapper* gw = nullptr);
	ObjectParticle(const ObjectParticle& other);

	void draw(GLenum mode = GL_TRIANGLES, const int& shaderProgramID = 0);
	void simulate(const float& timeStep, const glm::vec4& externalForce);

	void updateInternalPositions();
};