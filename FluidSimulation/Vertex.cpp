// Copyright (c) 2021 Mugurel-Adrian Enache

#include "Vertex.h"

bool Vertex::operator==(const Vertex& other) const
{
    return (position == other.position) && (normal == other.normal) && (color == other.color) && (uv == other.uv);
}
