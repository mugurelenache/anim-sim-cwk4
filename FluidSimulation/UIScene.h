// Copyright (c) 2021 Mugurel-Adrian Enache

#pragma once
#include <cstdint>

/**
 * @brief UIScene
 *
 * Contains the width and height as main members
 * They know what is the size of the specific scene.
 */
class UIScene
{
public:
	uint32_t width;
	uint32_t height;

public:
	// Default Constructor
	UIScene(uint32_t width, uint32_t height);

	// Pure virtual render
	// It should render the UIScene
	virtual void render() = 0;
};
