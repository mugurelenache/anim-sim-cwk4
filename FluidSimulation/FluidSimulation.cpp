// Copyright (c) 2020 Mugurel-Adrian Enache

#include <iostream>
#include "Renderer.h"

/**
 * @brief Runs the Fluid Simulation program
 * @return integer 0 if succeeded
*/
int main()
{
	// Creating a renderer and starting it
	auto renderer = std::make_unique<Renderer>();
	renderer->generalInit();
	renderer->initUI();
	renderer->renderLoop();
	return 0;
}
