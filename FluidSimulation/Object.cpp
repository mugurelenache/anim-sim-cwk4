// Copyright (c) 2021 Mugurel-Adrian Enache

#include "Object.h"
#include "GlobalsWrapper.h"

/**
 * @brief Object constructor, initializes transform
 * @param globals
*/
Object::Object(GlobalsWrapper* globals)
{
	transform = Transform(glm::mat4(1.0f), this);
	this->globals = &*globals;
}

Object::Object(const Object& other)
{
	this->globals = other.globals;
	this->transform = other.transform;
}

/**
 * @brief Destructor, does not destroy/free pointers
*/
Object::~Object()
{
}
