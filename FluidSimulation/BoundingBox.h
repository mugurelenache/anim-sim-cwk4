// Copyright (c) 2021 Mugurel-Adrian Enache

#pragma once
#include "Collider3D.h"
#include <glm/glm.hpp>
#include <vector>
#include "Vertex.h"

/**
 * @brief BoundingBox
 *
 * Contains the min/max coordinates given a list of vertices.
 * Usually kept by a mesh.
*/
class BoundingBox : public Collider3D
{
public:
	float width = 0.0f;
	float height = 0.0f;
	float depth = 0.0f;

public:
	float minX;
	float maxX;
	float minY;
	float maxY;
	float minZ;
	float maxZ;

public:
	// Constructor
	BoundingBox(const glm::vec4& center = glm::vec4(0.0f), const float& width = 0.0f, const float& height = 0.0f, const float& depth = 0.0f, Object* parent = nullptr);
	virtual bool isInside(const glm::vec4& position) const;
	virtual bool isClose(const glm::vec4& position) const;
	virtual void updateViaCenter();
};