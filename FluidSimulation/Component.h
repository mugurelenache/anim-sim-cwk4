// Copyright (c) 2021 Mugurel-Adrian Enache

#pragma once
#include <memory>

// Forward declaration
class Object;

/**
 * @brief Component class
 *
 * Usually extends into multiple types of components
*/
class Component : public std::enable_shared_from_this<Component>
{
public:
	// Should be weak as the component does not own the parent
	// only the other way round (i.e. parent owns the component)
	//
	// TODO Fix in future to make it safer
	Object* parent = nullptr;

public:
	Component(Object* parent = nullptr);
	Component(const Component& other);
	~Component();
};
