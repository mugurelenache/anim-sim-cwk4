// Copyright (c) 2020 Mugurel-Adrian Enache

#pragma once
#include "Object.h"
#include "Mesh.h"
#include "RigidBody.h"
#include "Material.h"

class ObjectCollider : public Object
{
public:
	std::shared_ptr<Mesh> mesh;
	std::shared_ptr<Material> material;
	std::unique_ptr<RigidBody> rigidBody;
	std::unique_ptr<Collider3D> collider;
	bool isHighlighted = false;
	bool isStatic = false;

	bool shouldRotate = false;
	bool hasMaxHeightSet = false;
	float maxHeight = 0.0f;

	glm::mat4 prevTransform = glm::mat4(1.0f);

public:
	ObjectCollider(std::shared_ptr<Mesh> mesh, std::shared_ptr<Material> material, GlobalsWrapper* gw = nullptr);
	ObjectCollider(const ObjectCollider& other);

	void draw(GLenum mode = GL_TRIANGLES, const int& shaderProgramID = 0);
	void simulate(const float& timeStep, const glm::vec4& externalForce);

	void performAction(const float& dt);
	glm::vec4 calculateFrictionAtPoint(const glm::vec4& point, const glm::vec4& externalForce);

};