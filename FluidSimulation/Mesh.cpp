// Copyright (c) 2021 Mugurel-Adrian Enache

#include "Mesh.h"

/**
 * @brief Creates a mesh given a VBO and an IBO
 * @param vertexBuffer
 * @param indexBuffer
*/
Mesh::Mesh(std::shared_ptr<VertexBufferObject<Vertex>> vertexBuffer, std::shared_ptr<IndexBufferObject<uint32_t>> indexBuffer)
{
	this->vertexBuffer = vertexBuffer;
	this->indexBuffer = indexBuffer;
	vao = std::make_unique<VertexArrayObject<Vertex, uint32_t>>(this->vertexBuffer, this->indexBuffer);
}

/**
 * @brief Generates the VAO
 * @param usage
*/
void Mesh::generateVAO(GLenum usage)
{
	vao->generate();
	vertexBuffer->generate();

	// Adds the VBO Attributes
	vertexBuffer->addVertexAttribute(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<GLvoid*>(0));
	vertexBuffer->addVertexAttribute(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<GLvoid*>(sizeof(glm::vec4)));
	vertexBuffer->addVertexAttribute(2, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<GLvoid*>(2 * sizeof(glm::vec4)));
	vertexBuffer->addVertexAttribute(3, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<GLvoid*>(3 * sizeof(glm::vec4)));

	indexBuffer->generate();

	// Binds the VAO
	vao->bind();

	vertexBuffer->bind();
	vertexBuffer->load(usage);
	vertexBuffer->activateAttributes();
	vertexBuffer->unbind();

	indexBuffer->bind();
	indexBuffer->load();

	vao->unbind();
}

/**
 * @brief Creates default triangle vertices
 * @return VBO containing vertices
*/
VertexBufferObject<Vertex> Mesh::generateDefaultTriangleVertices()
{
	Vertex a;
	Vertex b;
	Vertex c;

	a.position = glm::vec4(-0.5f, 0.0f, 0.0f, 1.0f);
	b.position = glm::vec4(0.5f, 0.0f, 0.0f, 1.0f);
	c.position = glm::vec4(0.0f, 0.5f, 0.0f, 1.0f);

	std::vector<Vertex> vertices = { a, b, c };

	return VertexBufferObject<Vertex>(vertices);
}

/**
 * @brief Creates default triangle vertices
 * @return IBO containing vertices
*/
IndexBufferObject<uint32_t> Mesh::generateDefaultTriangleIndices()
{
	std::vector<uint32_t> indices = { 0, 1, 2 };
	return IndexBufferObject<uint32_t>(indices);
}

/**
 * @brief Creates default quad vertices
 * @return VBO containing vertices
*/
VertexBufferObject<Vertex> Mesh::generateDefaultQuadVertices()
{
	Vertex a;
	Vertex b;
	Vertex c;
	Vertex d;

	a.position = glm::vec4(0.5f, 0.5f, 0.0f, 1.0f);
	b.position = glm::vec4(0.5f, -0.5f, 0.0f, 1.0f);
	c.position = glm::vec4(-0.5f, -0.5f, 0.0f, 1.0f);
	d.position = glm::vec4(-0.5f, 0.5f, 0.0f, 1.0f);

	std::vector<Vertex> vertices = { a, b, c, d };

	return  VertexBufferObject<Vertex>(vertices);
}

/**
 * @brief Creates default quad vertices
 * @return IBO containing vertices
*/
IndexBufferObject<uint32_t> Mesh::generateDefaultQuadIndices()
{
	std::vector<uint32_t> indices = { 0, 1, 3, 1, 2, 3 };
	return IndexBufferObject<uint32_t>(indices);
}