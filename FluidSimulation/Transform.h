// Copyright (c) 2021 Mugurel-Adrian Enache

#pragma once
#include <glm/glm.hpp>
#include "Component.h"

/**
 * @brief Transform
 *
 * Contains the model transform. Usually kept by objects
 * that require scene manipulation.
*/
class Transform : public Component
{
public:
	glm::vec3 position = glm::vec3(0.0f);
	glm::vec3 rotation = glm::vec3(0.0f);
	glm::vec3 scale = glm::vec3(1.0f);

	glm::mat4 matrix = glm::mat4(1.0f);

public:
	// Constructor
	Transform(const glm::mat4& matrix = glm::mat4(1.0f), Object* parent = nullptr);
	~Transform();

	void setPosition(const glm::vec3& position);
	void setRotation(const glm::vec3& rotation);
	void setScale(const glm::vec3& scale);

	void applyTRS();

	void recalculateVectorsViaTransformMatrix();
};