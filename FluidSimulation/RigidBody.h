// Copyright (c) 2021 Mugurel-Adrian Enache

#pragma once
#include "Component.h"
#include "glm/glm.hpp"

/**
 * @brief Rigid body, used for physics simulation
*/
class RigidBody : public Component
{
public:
	glm::vec4 velocity = glm::vec4(0.0f);
	glm::vec4 acceleration = glm::vec4(0.0f);
	glm::vec4 force = glm::vec4(0.0f);
	float mass = 0.001f;

	glm::vec4 gravity = glm::vec4(0.0f, -9.8f, 0.0f, 0.0f);
	bool hasCollided = false;

	bool isClose = false;

public:
	RigidBody(const glm::vec4& velocity = glm::vec4(0.0f), const glm::vec4& acceleration = glm::vec4(0.0f), const glm::vec4& force = glm::vec4(0.0f), const float& mass = 0.001f, Object* parent = nullptr);
	RigidBody(const RigidBody& other);
	~RigidBody();

	void simulateExplicitEuler(const float& timeStep, const glm::vec4& externalForce);
	void simulateSemiExplicitEuler(const float& timeStep, const glm::vec4& externalForce);
	void simulateTrial(const float& timeStep, const glm::vec4& externalForce);
	void simulateVerlet(const float& timeStep, const glm::vec4& externalForce);
	void simulateLeapfrog(const float& timeStep, const glm::vec4& externalForce);
	void simulate(const float& timeStep, const glm::vec4& externalForce);


	void checkCollision();
	void checkClose();
};

