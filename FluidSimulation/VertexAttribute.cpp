// Copyright (c) 2021 Mugurel-Adrian Enache

#include "VertexAttribute.h"

/**
 * @brief Creates a placeholder for a vertex attribute so that it can be turned asynchronously
 *
 * It is also known in advance.
 *
 * @param index
 * @param size
 * @param type
 * @param normalized
 * @param stride
 * @param offsetPtr
*/
VertexAttribute::VertexAttribute(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, GLvoid* offsetPtr)
{
	this->index = index;
	this->size = size;
	this->type = type;
	this->normalized = normalized;
	this->stride = stride;
	this->offsetPtr = offsetPtr;
}

/**
 * @brief Activates the vertex attribute pointer
 *
 * Must be called within a vertex buffer bind
*/
void VertexAttribute::activate()
{
	glVertexAttribPointer(index, size, type, normalized, stride, offsetPtr);
	glEnableVertexAttribArray(index);
}
