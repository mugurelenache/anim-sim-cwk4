// Copyright (c) 2021 Mugurel-Adrian Enache

#include "ObjectLight.h"

/**
 * @brief ObjectLight constructor, assigns the light properties
 * @param lightProperties
*/
ObjectLight::ObjectLight(LightProperties* lightProperties)
{
	properties.parent = this;

	if (lightProperties)
	{
		properties.ambient = lightProperties->ambient;
		properties.diffuse = lightProperties->diffuse;
		properties.specular = lightProperties->specular;
		properties.specularPower = lightProperties->specularPower;
	}
}

/**
 * @brief Simply uploads the light data in the shaders
 * @param shaderProgramID
*/
void ObjectLight::draw(const int& shaderProgramID)
{
	glUniform4fv(glGetUniformLocation(shaderProgramID, "lightProperties.ambient"), 1, &properties.ambient[0]);
	glUniform4fv(glGetUniformLocation(shaderProgramID, "lightProperties.diffuse"), 1, &properties.diffuse[0]);
	glUniform4fv(glGetUniformLocation(shaderProgramID, "lightProperties.specular"), 1, &properties.specular[0]);
	glUniform1f(glGetUniformLocation(shaderProgramID, "lightProperties.specularPower"), properties.specularPower);

	glUniform4fv(glGetUniformLocation(shaderProgramID, "lightPos"), 1, &transform.position[0]);
}
