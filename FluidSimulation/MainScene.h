// Copyright (c) 2021 Mugurel-Adrian Enache

#pragma once
#include "UIScene.h"
#include <examples/imgui_impl_glfw.h>
#include <examples/imgui_impl_opengl3.h>
#include "imfilebrowser.h"

#include "GlobalsWrapper.h"
#include "MeshLoader.h"

/**
 * @brief MainScene
 *
 * Responsible for drawing the UI elements
 * for the main scene. It includes the file browsers
 * used for opening/saving the files, as well as a mesh loader.
*/
class MainScene : public UIScene
{
public:
	std::shared_ptr<GlobalsWrapper> globals;

	ImGui::FileBrowser fileBrowser;
	ImGui::FileBrowser saveFileBrowser;
	std::unique_ptr<MeshLoader> meshLoader;

	ImGui::FileBrowser texFileBrowser;

public:
	// Frame buffer texture we render to
	uint32_t* renderingTexture;

	ImVec2 topBarSize ;
	ImVec2 topBarPosition;

	ImVec2 mainMenuSize;
	ImVec2 mainMenuPosition;

	ImVec2 renderingSize;
	ImVec2 renderingPosition;

	ImVec2 animControlsSize;
	ImVec2 animControlsPosition;

	ImVec2 ikControlsMenuSize;
	ImVec2 ikControlsMenuPosition;

	bool shouldPerformDamped = false;
	float lambda = 0.0f;

	bool dragging = false;
	ImVec2 dragVals = ImVec2(0.f, 0.f);

	// Controller var for object transform
	float objectRotation[3] = { 0, 0, 0 };

	// Controller vars for cage generation
	int cageGenType = 0;
	int cageGenParams2D[2] = { 1, 1 };
	int cageGenParams3D[3] = { 2, 2, 2 };
	int cageGenControlPoints2D = 5;
	int hasClickedOnGenerateCageButton = 0;

	bool hasBeenCentered = false;
	glm::mat4 trnsMatrix = glm::mat4(1.0f);
	glm::mat4 rotMat = glm::mat4(1.0f);
	bool hasClicked = false;


	float cameraRotation[3] = { 0, 0, 0 };


	// Animation Controls
	bool isPlaying = false;
	bool shouldPlayAnimFromStart = true;
	int sliderFrame = 0;
	uint32_t currentAnimFrame = 0;

public:
	// Constructor
	MainScene(uint32_t width, uint32_t height, uint32_t& renderingTexture);

	// Implementation of the render function
	virtual void render();

private:
	void showTopBar();
	void showMainMenu();
	void showRendering();
	void processMouse();

	void showLightData();
	void showSimulationData();
	void showCameraData();
	void showContainerData();
	void showSimulationControls();
};

