// Copyright (c) 2021 Mugurel-Adrian Enache

#pragma once
#include <cstdint>
#include <string>
#include <vector>

namespace util
{
	uint32_t loadTexture(const std::string& path);
	void saveFrame(const std::string& path, const int& width, const int& height, const std::vector<unsigned int>& buffer);

	template<typename Base, typename T>
	inline bool instanceof(const T*)
	{
		return std::is_base_of<Base, T>::value;
	}
};