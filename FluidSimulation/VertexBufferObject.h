// Copyright (c) 2021 Mugurel-Adrian Enache

#pragma once
#include <vector>
#include <GL/gl3w.h>
#include "VertexAttribute.h"

/**
 * @brief VertexBufferObject
 *
 * Responsible for holding vertex data.
 * @tparam T
*/
template <class T>
class VertexBufferObject
{
public:
	uint32_t id = 0;
	std::vector<T> elements;
	std::vector<VertexAttribute> attributes;

public:
	VertexBufferObject<T>(uint32_t size, void* data);
	VertexBufferObject<T>(const std::vector<T>& data);
	VertexBufferObject<T>(const VertexBufferObject& other);
	VertexBufferObject<T>(VertexBufferObject&& other) noexcept;

	void generate();
	void load(GLenum usage = GL_STATIC_DRAW);
	void bind();
	void unbind();
	void draw(GLenum mode = GL_TRIANGLES);
	void activateAttributes();
	void addVertexAttribute(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, GLvoid* offsetPtr);
	T& operator[](const uint32_t& i);
	void update(uint32_t offset, GLvoid* data, bool propagateOnGPU = true);
	void updateGPUData();
};

template<class T>
inline VertexBufferObject<T>::VertexBufferObject(uint32_t size, void* data)
{
	for (uint32_t i = 0; i < size; i++)
	{
		elements.push_back(static_cast<T*>(data)[i]);
	}
}

template<class T>
inline VertexBufferObject<T>::VertexBufferObject(const std::vector<T>& data)
{
	for (size_t i = 0; i < data.size(); i++)
	{
		elements.push_back(data[i]);
	}
}

template<class T>
inline VertexBufferObject<T>::VertexBufferObject(const VertexBufferObject& other)
{
	this->id = other.id;
	this->elements = other.elements;
	this->attributes = other.attributes;
}

template<class T>
inline VertexBufferObject<T>::VertexBufferObject(VertexBufferObject&& other) noexcept
{
	this->id = other.id;
	this->elements = other.elements;
	this->attributes = other.attributes;
}

template<class T>
inline void VertexBufferObject<T>::generate()
{
	glGenBuffers(1, &id);
}

template<class T>
inline void VertexBufferObject<T>::load(GLenum usage)
{
	glBufferData(GL_ARRAY_BUFFER, elements.size() * sizeof(T), elements.data(), usage);
}

template<class T>
inline void VertexBufferObject<T>::bind()
{
	glBindBuffer(GL_ARRAY_BUFFER, id);
}

template<class T>
inline void VertexBufferObject<T>::unbind()
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

template<class T>
inline void VertexBufferObject<T>::draw(GLenum mode)
{
	glDrawArrays(mode, 0, static_cast<GLsizei>(elements.size()));
}

template<class T>
inline void VertexBufferObject<T>::activateAttributes()
{
	for (auto& attr : attributes)
	{
		attr.activate();
	}
}

template<class T>
inline void VertexBufferObject<T>::addVertexAttribute(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, GLvoid* offsetPtr)
{
	auto va = VertexAttribute(index, size, type, normalized, stride, offsetPtr);
	attributes.push_back(va);
}

template<class T>
inline T& VertexBufferObject<T>::operator[](const uint32_t& i)
{
	return elements[i];
}

template<class T>
inline void VertexBufferObject<T>::update(uint32_t offset, GLvoid* data, bool propagateOnGPU)
{
	if (propagateOnGPU)
	{
		bind();
		glBufferSubData(GL_ARRAY_BUFFER, sizeof(T) * offset, sizeof(T), data);
		unbind();
	}

	elements[offset] = *(reinterpret_cast<T*>(data));
}

template<class T>
inline void VertexBufferObject<T>::updateGPUData()
{
	bind();
	glBufferData(GL_ARRAY_BUFFER, sizeof(T) * elements.size(), elements.data(), GL_DYNAMIC_DRAW);
	unbind();
}
