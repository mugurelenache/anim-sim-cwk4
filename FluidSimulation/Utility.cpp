// Copyright (c) 2020 Mugurel-Adrian Enache

#include "Utility.h"
#include <iostream>
#include <string>
#include <GL/gl3w.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#define STBI_MSC_SECURE_CRT
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>
#include <filesystem>

/**
 * @brief Saves a buffer in a jpg
 * @param path
 * @param width
 * @param height
 * @param buffer
*/
void util::saveFrame(const std::string& path, const int& width, const int& height, const std::vector<unsigned int>& buffer)
{
	stbi_flip_vertically_on_write(1);
	stbi_write_jpg(path.c_str(), width, height, 3, buffer.data(), width * 3);
}

/**
 * @brief Loads a texture from path
 * @param path
 * @return Return the created texture's ID
*/
uint32_t util::loadTexture(const std::string& path)
{
	uint32_t texID;
	glGenTextures(1, &texID);

	int w;
	int h;
	int componentsCount;

	stbi_uc* data = stbi_load(path.c_str(), &w, &h, &componentsCount, 0);

	if (data)
	{
		GLenum format;

		if (componentsCount == 1)
		{
			format = GL_RED;
		}
		else if (componentsCount == 3)
		{
			format = GL_RGB;
		}
		else if (componentsCount == 4)
		{
			format = GL_RGBA;
		}
		else
		{
			format = GL_RGBA;
			assert(false && "Error: Texture components not compatible with implementation.");
		}

		glBindTexture(GL_TEXTURE_2D, texID);
		glTexImage2D(GL_TEXTURE_2D, 0, format, w, h, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		stbi_image_free(data);
	}
	else
	{
		std::cerr << "Error: Failed to load texture at: " << path << std::endl;
		stbi_image_free(data);
	}

	return texID;
}