#pragma once
#include "Object.h"
#include "Material.h"
#include "RigidBody.h"

class ObjectContainer : public Object
{
public:
	std::shared_ptr<Mesh> mesh;
	std::shared_ptr<Material> material;
	std::unique_ptr<Collider3D> collider;

	// In meters
	float width = 1.0f;
	float height = 1.0f;

public:
	ObjectContainer(std::shared_ptr<Mesh> mesh, std::shared_ptr<Material> material, GlobalsWrapper* gw = nullptr);
	ObjectContainer(const ObjectContainer& other);

	void draw(GLenum mode = GL_TRIANGLES, const int& shaderProgramID = 0);
};

