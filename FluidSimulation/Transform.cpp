// Copyright (c) 2021 Mugurel-Adrian Enache

#include "Transform.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/matrix.hpp>
#include <iostream>
#include <glm/gtx/matrix_decompose.hpp>


/**
 * @brief Creates a matrix given another one
 * @param matrix
*/
Transform::Transform(const glm::mat4& matrix, Object* parent) : Component(parent)
{
	this->matrix = matrix;
}

/**
 * @brief Destructor, does not delete anything
*/
Transform::~Transform()
{
	// Don't delete anything
}

/**
 * @brief Sets the position then applies TRS
 * @param position
*/
void Transform::setPosition(const glm::vec3& position)
{
	this->position = position;
	applyTRS();
}

/**
 * @brief Sets the rotation then applies TRS
 * @param rotation
*/
void Transform::setRotation(const glm::vec3& rotation)
{
	this->rotation = rotation;
	applyTRS();
}

/**
 * @brief Sets the scale then applies TRS
 * @param scale
*/
void Transform::setScale(const glm::vec3& scale)
{
	this->scale = scale;
	applyTRS();
}

/**
 * @brief Applies TRS by creating an identity matrix, translating it, rotating then scaling it.
*/
void Transform::applyTRS()
{
	matrix = glm::mat4(1.0f);

	matrix = glm::translate(matrix, position);
	matrix = glm::rotate(matrix, glm::radians(rotation.x), glm::vec3(1, 0, 0));
	matrix = glm::rotate(matrix, glm::radians(rotation.y), glm::vec3(0, 1, 0));
	matrix = glm::rotate(matrix, glm::radians(rotation.z), glm::vec3(0, 0, 1));
	matrix = glm::scale(matrix, scale);
}

/**
 * @brief Resets the vectors by decomposing the matrix transformation
*/
void Transform::recalculateVectorsViaTransformMatrix()
{
	glm::quat qrotation;
	glm::vec3 skew;
	glm::vec4 perspective;

	glm::decompose(matrix, scale, qrotation, position, skew, perspective);
	rotation = glm::eulerAngles(qrotation);
}
