// Copyright (c) 2021 Mugurel-Adrian Enache

#pragma once
#include <glm/glm.hpp>
#include <glm/gtx/hash.hpp>

/**
 * @brief Vertex
 *
 * Contains data about a vertex.
*/
class Vertex
{
public:
	glm::vec4 position = glm::vec4(0.0f);
	glm::vec4 normal = glm::vec4(0.0f);
	glm::vec4 color = glm::vec4(1.0f);
	glm::vec2 uv = glm::vec2(0.0f);

public:
	// Default constructor
	Vertex() = default;
	bool operator==(const Vertex& other) const;
};

namespace std
{
	template<>
	struct hash<Vertex>
	{
		// TODO Change to boost version of hashing (hashCombine)
		size_t operator()(const Vertex& v) const
		{
			return
				(
					((hash<glm::vec4>()(v.position) ^ hash<glm::vec4>()(v.normal) << 1) >> 1) ^
					((hash<glm::vec4>()(v.color) ^ hash<glm::vec2>()(v.uv) << 1) >> 1)
				) >> 1;
		}
	};
}

