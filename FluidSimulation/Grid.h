// Copyright (c) 2021 Mugurel-Adrian Enache

#pragma once
#include "Component.h"
#include <glm/glm.hpp>
#include <vector>
#include "Mesh.h"
#include "Vertex.h"

class Grid : public Component
{
public:
	struct Cell
	{
		std::vector<uint32_t> particles;
		bool isSurface = false;
		float colorField = 0.0f;

		Cell() = default;
		Cell(const Cell& other) : particles(other.particles), isSurface(other.isSurface), colorField(other.colorField) {} ;
	};

	glm::vec4 startPosition = glm::vec4(0.0f);
	float width = 0.0f;
	float height = 0.0f;
	float cellWidth = 0.0f;
	float cellHeight = 0.0f;
	std::vector<Cell> cells;
	std::vector<Vertex> vertices;
	std::unique_ptr<Mesh> mesh;
	bool visible = false;

public:
	Grid(const glm::vec4& startPosition = glm::vec4(0.0f), const float& cellWidth = 1.0f, const float& cellHeight = 1.0f, const float& width = 0.0f, const float& height = 0.0f, Object* parent = nullptr);
	Grid(const Grid& other);
	void create();

	int getCellAtPosition(const glm::vec4& position);
	bool isSurfaceCell(const uint32_t& cellID);
	uint32_t getCellsX();
	uint32_t getCellsY();
	void resetCells();

	std::vector<uint32_t> getNeighboringCellsOfCell(const uint32_t& cellID);
	std::vector<uint32_t> getParticlesInCells(const std::vector<uint32_t>& cellIDs);

	void draw(const int& shaderProgramID);
};

