// Copyright (c) 2021 Mugurel-Adrian Enache

#include "MainScene.h"
#include "imgui.h"
#include "imgui_internal.h"
#include <iostream>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/ext/matrix_projection.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Utility.h"

/**
 * @brief Creates the main scene that will render the framebuffer color texture
 * @param width
 * @param height
 * @param renderingTexture
*/
MainScene::MainScene(uint32_t width, uint32_t height, uint32_t& renderingTexture) : UIScene(width, height)
{
	// Initializing the file browsers for open file and save file
	fileBrowser = ImGui::FileBrowser(ImGuiFileBrowserFlags_CloseOnEsc);
	fileBrowser.SetTitle("Open File");
	fileBrowser.SetTypeFilters({ ".obj" });

	saveFileBrowser = ImGui::FileBrowser(ImGuiFileBrowserFlags_EnterNewFilename |
		ImGuiFileBrowserFlags_CloseOnEsc | ImGuiFileBrowserFlags_CreateNewDir);

	saveFileBrowser.SetTitle("Save File");
	saveFileBrowser.SetTypeFilters({ ".obj" });

	texFileBrowser = ImGui::FileBrowser(ImGuiFileBrowserFlags_CloseOnEsc);
	texFileBrowser.SetTitle("Open File");
	texFileBrowser.SetTypeFilters({ ".png", ".jpg" });

	this->renderingTexture = &renderingTexture;

	// Creates a mesh loader
	meshLoader = std::make_unique<MeshLoader>();
}

/**
 * @brief Renders the main scene
*/
void MainScene::render()
{
	showTopBar();
	showMainMenu();
	showRendering();
	processMouse();
}

/**
 * @brief Shows the file menu top bar
*/
void MainScene::showTopBar()
{
	// Rendering the menu bar
	if (ImGui::BeginMainMenuBar())
	{
		topBarPosition = ImGui::GetWindowPos();
		topBarSize = ImGui::GetWindowSize();

		if (ImGui::BeginMenu("File"))
		{
			ImGui::Text("Nothing to see here");
			ImGui::EndMenu();
		}
		ImGui::EndMainMenuBar();
	}
}

/**
 * @brief Shows the main menu
*/
void MainScene::showMainMenu()
{
	// Creates the window for the menu
	ImGuiIO& io = ImGui::GetIO();
	ImGuiWindowFlags flags = ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings |
		ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_HorizontalScrollbar;

	mainMenuPosition = ImVec2(topBarPosition.x + 10.f, topBarPosition.y + topBarSize.y + 10.f);
	mainMenuSize = ImVec2(400.f, height - mainMenuPosition.y - 10.f);
	ImVec2 pivot = ImVec2(0.f, 0.f);

	ImGui::SetNextWindowSize(mainMenuSize, ImGuiCond_Always);
	ImGui::SetNextWindowPos(mainMenuPosition, ImGuiCond_Always, pivot);

	if (ImGui::Begin("Menu", nullptr, flags))
	{
		showSimulationData();
		ImGui::Separator();
		showContainerData();
		showCameraData();
		showLightData();
		ImGui::Separator();
		showSimulationControls();

		mainMenuSize = ImGui::GetWindowSize();
		ImGui::End();
	}
}

/**
 * @brief Displays the color texture in a window
*/
void MainScene::showRendering()
{
	ImGuiIO& io = ImGui::GetIO();

	ImGuiWindowFlags flags = ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoMove |
		ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoNav | ImGuiWindowFlags_NoDecoration |
		ImGuiWindowFlags_NoNavInputs | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_AlwaysAutoResize;

	float minWidth = 400.f;
	ImGuiStyle& style = ImGui::GetStyle();

	auto oldStyle = style.WindowPadding;

	style.WindowPadding = ImVec2(0, 0);

	// Computes the margins
	if (mainMenuPosition.x + mainMenuSize.x + 10.f >= minWidth)
	{
		minWidth = mainMenuPosition.x + mainMenuSize.x + 10.f;
	}

	renderingPosition = ImVec2(minWidth, mainMenuPosition.y);
	renderingSize = ImVec2(width - renderingPosition.x - 10.f, height - renderingPosition.y - 10.f);
	ImVec2 pivot = ImVec2(0.f, 0.f);

	ImGui::SetNextWindowPos(renderingPosition, ImGuiCond_Always, pivot);
	ImGui::SetNextWindowSize(renderingSize, ImGuiCond_Always);

	bool tru = true;

	if (ImGui::Begin("Renderer Output", &tru, flags))
	{
		ImGui::BeginChild("GameRender", ImVec2(0, 0), false, flags);
		ImGui::Image(reinterpret_cast<void*>(*renderingTexture), renderingSize, ImVec2(0, 1), ImVec2(1, 0));
		ImGui::EndChild();
	}
	ImGui::End();

	style.WindowPadding = oldStyle;

	// Fixes projection here
	float aspect = renderingSize.x / renderingSize.y;
	globals->camera->projection = glm::perspective(glm::radians(60.0f), aspect, 0.001f, 500.f);
	globals->camera->transform.matrix = glm::lookAt(globals->camera->transform.position, globals->camera->transform.position + globals->camera->forward, globals->camera->up);
}

/**
 * @brief Performs mouse processing
*/
void MainScene::processMouse()
{
	ImVec2 mouseCoords = ImGui::GetMousePos();
	ImGuiStyle& style = ImGui::GetStyle();

	// Stores the coordinates for the inner part of the rendering widget
	float minX = renderingPosition.x;
	float maxX = renderingPosition.x + renderingSize.x;
	float minY = renderingPosition.y;
	float maxY = renderingPosition.y + renderingSize.y;

	// Rendering widget size
	float tempWidth = maxX - minX;
	float tempHeight = maxY - minY;

	float scaleWidth = globals->renderWidth / tempWidth;
	float scaleHeight = globals->renderHeight / tempHeight;

	globals->mousePosX = (mouseCoords.x - minX) * scaleWidth;
	globals->mousePosY = globals->renderHeight - (mouseCoords.y - minY) * scaleHeight;

	// If the mouse is within the rendering bounds
	if (mouseCoords.x > minX && mouseCoords.x < maxX && mouseCoords.y > minY && mouseCoords.y < maxY)
	{
		dragging = ImGui::IsMouseDragging(ImGuiMouseButton_Left);
		bool mouseDown = ImGui::IsMouseDown(ImGuiMouseButton_Left);

		ImVec2 delta = ImGui::GetMouseDragDelta(ImGuiMouseButton_Left);

		glm::vec3 screenPos = glm::vec3(globals->mousePosX, globals->mousePosY, globals->mouseDepthValue);
		auto unproj = glm::unProject(screenPos, globals->camera->transform.matrix, globals->camera->projection, glm::vec4(0, 0, globals->renderWidth, globals->renderHeight));

		if (!mouseDown)
		{
			if (globals->selectedRigidBody >= 0)
			{
				globals->selectedRigidBody = -1;
			}
		}

		ImGui::ResetMouseDragDelta();
	}
	else
	{
		globals->isMouseInFramebuffer = false;
	}


}

/**
 * @brief Shows light data, including light transforms and properties
*/
void MainScene::showLightData()
{
	if (ImGui::CollapsingHeader("Light Controls"))
	{
		if (globals->lightObject)
		{
			std::string treeID = "LightTransform";
			if (ImGui::TreeNode(treeID.c_str(), "Transform"))
			{
				if (ImGui::DragFloat3("Position", glm::value_ptr(globals->lightObject->transform.position), 0.01f))
				{
					globals->lightObject->transform.applyTRS();
				}

				if (ImGui::DragFloat3("Rotation", glm::value_ptr(globals->lightObject->transform.rotation), 0.01f))
				{
					globals->lightObject->transform.applyTRS();
				}

				ImGui::TreePop();
			}

			treeID = "LightProprieties";
			if (ImGui::TreeNode(treeID.c_str(), "Color"))
			{
				ImGui::DragFloat3("Ambient", glm::value_ptr(globals->lightObject->properties.ambient), 0.001f, 0.0f, 1.0f);
				ImGui::DragFloat3("Diffuse", glm::value_ptr(globals->lightObject->properties.diffuse), 0.001f, 0.0f, 1.0f);
				ImGui::DragFloat3("Specular", glm::value_ptr(globals->lightObject->properties.specular), 0.001f, 0.0f, 1.0f);
				ImGui::DragFloat("Specular Exponent", &(globals->lightObject->properties.specularPower), 0.001f, 0.0f, 1.0f);
				ImGui::TreePop();
			}
		}
		else
		{
			ImGui::TextColored(ImVec4(1, 0, 0, 1), "No settings available.");
			ImGui::TextColored(ImVec4(1, 0, 0, 1), "Load an .obj file from the file-menu available.");
		}
	}
}

/**
 * @brief Shows the simulated data, including texture configs & colliders
*/
void MainScene::showSimulationData()
{
	texFileBrowser.Display();
	if (ImGui::CollapsingHeader("Simulation Data"))
	{
		std::string treeID = "Fluid Transform";
		if (ImGui::TreeNode(treeID.c_str(), treeID.c_str()))
		{
			if (ImGui::DragFloat3("Position", glm::value_ptr(globals->fluidObject->transform.position), 0.01f))
			{
				globals->fluidObject->transform.applyTRS();
			}

			if (ImGui::DragFloat3("Rotation", glm::value_ptr(globals->fluidObject->transform.rotation), 0.01f))
			{
				globals->fluidObject->transform.applyTRS();
			}

			ImGui::TreePop();
		}


		std::string treeIDC = "Particles: " + std::to_string(globals->fluidObject->particles.size());
		if (ImGui::TreeNode(treeIDC.c_str(), treeIDC.c_str()))
		{
			for (uint32_t i = 0; i < globals->fluidObject->particles.size(); i++)
			{
				std::string treeIDs = "Particle " + std::to_string(i);

				if (ImGui::TreeNode(treeIDs.c_str(), treeIDs.c_str()))
				{
					ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
					ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);

					ImGui::DragFloat3("Position", glm::value_ptr(globals->fluidObject->particles[i].transform.position), 0.01f);

					ImGui::DragFloat("Initial Density", &globals->fluidObject->particles[i].initialDensity, 1.0f, 0.0f, 0.0f, "%.10f");
					ImGui::DragFloat("Density", &globals->fluidObject->particles[i].density, 1.0f, 0.0f, 0.0f, "%.10f");
					ImGui::DragFloat("Pressure", &globals->fluidObject->particles[i].pressure, 1.0f, 0.0f, 0.0f, "%.10f");
					ImGui::DragFloat("Mass", &globals->fluidObject->particles[i].rigidBody->mass);


					ImGui::PopItemFlag();
					ImGui::PopStyleVar();



					//ImGui::Checkbox("Highlight", &globals->fluidObject->particles[i].isHighlighted);

					ImGui::TreePop();
				}
			}

			ImGui::TreePop();
		}
	}
}

/**
 * @brief Show camera transform data
*/
void MainScene::showCameraData()
{
	if (ImGui::CollapsingHeader("Camera Controls"))
	{
		std::string treeID = "Transform Camera";
		if (ImGui::TreeNode(treeID.c_str(), "Transform"))
		{
			if (ImGui::DragFloat3("Position", glm::value_ptr(globals->camera->transform.position), 0.01f))
			{
				globals->camera->transform.applyTRS();
			}
			if (ImGui::DragFloat3("Rotation", glm::value_ptr(globals->camera->transform.rotation), 0.01f))
			{
				// INITIAL FWD VECTOR
				glm::vec3 rot = glm::vec3(0, 0, -1);

				rot = glm::rotateX(rot, glm::radians(globals->camera->transform.rotation.x));
				rot = glm::rotateY(rot, glm::radians(globals->camera->transform.rotation.y));
				rot = glm::rotateZ(rot, glm::radians(globals->camera->transform.rotation.z));
				globals->camera->forward = rot;
				globals->camera->transform.applyTRS();
			}

			ImGui::TreePop();
		}
	}
}

/**
 * @brief Show scene data,
*/
void MainScene::showContainerData()
{
	if (ImGui::CollapsingHeader("Container Data"))
	{
		if (globals->containerObject)
		{
			std::string treeID = "Transform";

			if (ImGui::TreeNode(treeID.c_str(), treeID.c_str()))
			{
				if (ImGui::DragFloat3("Position", glm::value_ptr(globals->containerObject->transform.position), 0.01f))
				{
					globals->containerObject->transform.applyTRS();
					globals->fluidObject->reset();
				}
				if (ImGui::DragFloat3("Rotation", glm::value_ptr(globals->containerObject->transform.rotation), 0.01f))
				{
					globals->containerObject->transform.applyTRS();
					globals->fluidObject->reset();
				}

				ImGui::TreePop();
			}
		}
		else
		{
			ImGui::TextColored(ImVec4(1, 0, 0, 1), "No container in the scene.");
		}
	}
}

/**
 * @brief Shows the simulation controls, records, coefficients, etc.
*/
void MainScene::showSimulationControls()
{
	if (ImGui::CollapsingHeader("Simulation Controls"))
	{
		if (globals->fluidObject)
		{
			ImGui::Checkbox("Show Grid", &globals->fluidObject->grid->visible);
			bool shouldResetFluidSystem = false;

			shouldResetFluidSystem |= ImGui::DragFloat("Particle Radius", &globals->fluidObject->generalParticleRadius, 0.001f, 0.04f, 10.f, "%.03f m");
			shouldResetFluidSystem |= ImGui::DragFloat("Fluid Width", &globals->fluidObject->width, 0.001f, 0.0f, 20.f, "%.03f m");
			shouldResetFluidSystem |= ImGui::DragFloat("Fluid Height", &globals->fluidObject->height, 0.001f, 0.0f, 20.f, "%.03f m");

			bool shouldResizeContainer = false;
			shouldResizeContainer |= ImGui::DragFloat("Tank Width", &globals->containerObject->width, 0.001f, 0.0f, 10.f, "%.03f m");
			shouldResizeContainer |= ImGui::DragFloat("Tank Height", &globals->containerObject->height, 0.001f, 0.0f, 10.f, "%.03f m");

			if (shouldResizeContainer)
			{
				globals->containerObject->transform.setScale(glm::vec3(globals->containerObject->width, globals->containerObject->height, 1));
				globals->fluidObject->reset();
			}

			ImGui::Separator();

			ImGui::DragFloat("Step Size", &globals->fluidObject->stepSize, 0.001f, 0.0f, 10.f, "%.08f");

			shouldResetFluidSystem |= ImGui::Combo("Integration Scheme", &globals->fluidObject->integrationID, globals->fluidObject->integrationSchemes, IM_ARRAYSIZE(globals->fluidObject->integrationSchemes), IM_ARRAYSIZE(globals->fluidObject->integrationSchemes));
			shouldResetFluidSystem |= ImGui::DragFloat("Radius Multiplier", &globals->fluidObject->radiusMultiplier, 0.01f, globals->fluidObject->generalParticleRadius, 100.f, "%.03f m");


			shouldResetFluidSystem |= ImGui::Combo("General Kernel", &globals->fluidObject->kernelID, globals->fluidObject->kernels, IM_ARRAYSIZE(globals->fluidObject->kernels), IM_ARRAYSIZE(globals->fluidObject->kernels));

			shouldResetFluidSystem |= ImGui::Combo("Internal Pressure Kernel", &globals->fluidObject->internalPressureKernelID, globals->fluidObject->internalPressureKernels, IM_ARRAYSIZE(globals->fluidObject->internalPressureKernels), IM_ARRAYSIZE(globals->fluidObject->internalPressureKernels));

			shouldResetFluidSystem |= ImGui::Checkbox("Apply Viscosity", &globals->fluidObject->shouldApplyViscosity);

			if (!globals->fluidObject->shouldApplyViscosity)
			{
				ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
				ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
			}
			shouldResetFluidSystem |= ImGui::DragFloat("Viscosity Coefficient (miu)", &globals->fluidObject->viscosityCoefficient, 0.1f, -200.f, 1000.f);

			if (!globals->fluidObject->shouldApplyViscosity)
			{
				ImGui::PopItemFlag();
				ImGui::PopStyleVar();
			}

			shouldResetFluidSystem |= ImGui::Checkbox("Apply Surface Tension", &globals->fluidObject->shouldApplySurfaceTension);

			if (!globals->fluidObject->shouldApplySurfaceTension)
			{
				ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
				ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
			}
			shouldResetFluidSystem |= ImGui::DragFloat("Surface Tension Coefficient", &globals->fluidObject->surfaceTensionCoefficient, 0.1f, -200.f, 200.f);

			if (!globals->fluidObject->shouldApplySurfaceTension)
			{
				ImGui::PopItemFlag();
				ImGui::PopStyleVar();
			}

			ImGui::Separator();

			bool shouldHide = globals->shouldSimulate;

			if (shouldHide)
			{
				ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
				ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
			}

			if (ImGui::Button("Simulate"))
			{
				globals->shouldSimulate = true;
			}

			if (shouldHide)
			{
				ImGui::PopItemFlag();
				ImGui::PopStyleVar();
			}

			ImGui::SameLine();

			if (ImGui::Button("Pause"))
			{
				globals->shouldSimulate = false;
			}

			ImGui::SameLine();

			if (ImGui::Button("Stop"))
			{
				globals->shouldSimulate = false;
				globals->fluidObject->reset();
			}

			ImGui::Separator();

			if (shouldResetFluidSystem)
			{
				globals->shouldSimulate = false;
				globals->fluidObject->reset();
			}

		}
		else
		{
			ImGui::TextColored(ImVec4(1, 0, 0, 1), "No simulation settings available.");
			ImGui::TextColored(ImVec4(1, 0, 0, 1), "Load an .obj file from the file-menu available.");
		}
	}
}
