// Copyright (c) 2021 Mugurel-Adrian Enache

#pragma once
#include "Component.h"
#include <glm/glm.hpp>
#include <GL/gl3w.h>

/**
 * @brief Collider3D doing is inside calculations
*/
class Collider3D : public Component
{
public:
	glm::vec4 center = glm::vec4(0.0f);
	float eps = 0.1f;

public:
	Collider3D(const glm::vec4& center = glm::vec4(0.0f), Object* parent = nullptr);
	virtual bool isInside(const glm::vec4& position) const = 0;
	virtual void updateViaCenter() = 0;
	virtual bool isClose(const glm::vec4& position) const = 0;
};

