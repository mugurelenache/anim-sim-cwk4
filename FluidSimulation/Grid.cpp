// Copyright (c) 2021 Mugurel-Adrian Enache

#include "Grid.h"
#include "Object.h"
#include "GlobalsWrapper.h"
#include <iostream>

Grid::Grid(const glm::vec4& startPosition, const float& cellWidth, const float& cellHeight, const float& width, const float& height, Object* parent) : Component(parent)
{
	this->startPosition = startPosition;
	this->cellWidth = cellWidth;
	this->cellHeight = cellHeight;
	this->width = width;
	this->height = height;
}

Grid::Grid(const Grid& other) : Component(other)
{
	startPosition = other.startPosition;
	cellWidth = other.cellWidth;
	cellHeight = other.cellHeight;
	width = other.width;
	height = other.height;
	cells = other.cells;
	vertices = other.vertices;
	visible = other.visible;

	const auto& ovbo = std::make_shared<VertexBufferObject<Vertex>>(*other.mesh->vertexBuffer.get());
	const auto& oibo = std::make_shared<IndexBufferObject<uint32_t>>(*other.mesh->indexBuffer.get());

	mesh.reset(new Mesh(ovbo, oibo));
}

void Grid::create()
{
	uint32_t cellsX = getCellsX();
	uint32_t cellsY = getCellsY();

	for (uint32_t y = 0; y <= cellsY; y++)
	{
		for (uint32_t x = 0; x <= cellsX; x++)
		{
			float yp = y * cellHeight;
			float xp = x * cellWidth;

			Vertex temp;
			temp.position = glm::vec4(xp, yp, 0, 0.0f) + startPosition;
			vertices.push_back(temp);
			cells.push_back(Cell());
		}
	}

	VertexBufferObject vbo = VertexBufferObject(vertices);
	std::vector<uint32_t> indices;

	for (float y = 0; y < cellsY; y++)
	{
		for (float x = 0; x < cellsX; x++)
		{
			indices.push_back(y * (cellsX + 1) + x);
			indices.push_back(y * (cellsX + 1) + x + 1);

			if (x == cellsX - 1)
			{
				indices.push_back(y * (cellsX + 1) + x + 1);
				indices.push_back((y + 1) * (cellsX + 1) + x + 1);
			}

			if (y == cellsY - 1)
			{
				indices.push_back((y + 1) * (cellsX + 1) + x + 1);
				indices.push_back((y + 1) * (cellsX + 1) + x);
			}

			indices.push_back((y + 1) * (cellsX + 1) + x);
			indices.push_back(y * (cellsX + 1) + x);
		}
	}

	IndexBufferObject ibo = IndexBufferObject(indices);

	mesh.reset(new Mesh(std::make_shared<VertexBufferObject<Vertex>>(vbo), std::make_shared<IndexBufferObject<uint32_t>>(ibo)));
	mesh->generateVAO();
}

int Grid::getCellAtPosition(const glm::vec4& position)
{
	glm::vec4 newpos = position;
	newpos.x -= startPosition.x;

	uint32_t posX = newpos.x / cellWidth;

	if (posX > getCellsX())
	{
		return -1;
	}

	newpos.y -= startPosition.y;

	uint32_t posY = newpos.y / cellHeight;

	if (posY > getCellsY())
	{
		return -1;
	}
	uint32_t pos = posX + posY * getCellsX();

	if (pos > getCellsX() * getCellsY())
	{
		return -1;
	}

	return pos;
}

bool Grid::isSurfaceCell(const uint32_t& cellID)
{
	if (cellID >= cells.size() || cellID < 0)
	{
		return false;
	}

	auto neighboringCells = getNeighboringCellsOfCell(cellID);

	for (size_t i = 0; i < neighboringCells.size(); i++)
	{
		if (cells[neighboringCells[i]].particles.size() == 0)
		{
			return true;
		}
	}
}

uint32_t Grid::getCellsX()
{
	assert(cellWidth != 0 && "Error: cell width is 0.");
	return width / cellWidth;
}

uint32_t Grid::getCellsY()
{
	assert(cellHeight != 0 && "Error: cell height is 0.");
	return height / cellHeight;
}

void Grid::resetCells()
{
	cells.clear();
	cells.resize(getCellsX() * getCellsY());
}

std::vector<uint32_t> Grid::getNeighboringCellsOfCell(const uint32_t& cellID)
{
	uint32_t cellsX = getCellsX();
	uint32_t cellsY = getCellsY();

	if (cellID >= cells.size() || cellID < 0)
	{
		return std::vector<uint32_t>();
	}

	uint32_t row = cellID / cellsX;
	uint32_t col = cellID % cellsX;

	uint32_t pos[9][2] = {
		{ row + 1, col - 1},
		{ row + 1, col},
		{row + 1, col + 1},
		{row, col - 1},
		{row, col},
		{row, col + 1},
		{row - 1, col - 1},
		{row - 1, col},
		{row - 1, col + 1}
	};

	std::vector<uint32_t> out;

	for (size_t i = 0; i < 9; i++)
	{
		if ((pos[i][0] < 0 || pos[i][0] >= cellsY) || (pos[i][1] < 0 || pos[i][1] >= cellsX))
		{
			continue;
		}
		else
		{
			uint32_t cid = cellsX * pos[i][0] + pos[i][1];

			if (cid <= cellsX * cellsY)
			{
				out.push_back(cellsX * pos[i][0] + pos[i][1]);
			}
		}
	}

	return out;
}

std::vector<uint32_t> Grid::getParticlesInCells(const std::vector<uint32_t>& cellIDs)
{
	std::vector<uint32_t> out;

	for (size_t i = 0; i < cellIDs.size(); i++)
	{
		for (size_t j = 0; j < cells[cellIDs[i]].particles.size(); j++)
		{
			out.push_back(cells[cellIDs[i]].particles[j]);
		}
	}

	return out;
}

void Grid::draw(const int& shaderProgramID)
{
	if (visible)
	{
		glm::mat4 identity = glm::mat4(1.0f);
		glm::vec4 emission = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
		glUniform4fv(glGetUniformLocation(shaderProgramID, "objectMaterial.emission"), 1, &emission[0]);
		glUniformMatrix4fv(glGetUniformLocation(shaderProgramID, "model"), 1, GL_FALSE, (&identity[0][0]));

		mesh->vao->bind();
		mesh->vao->draw(GL_LINES);
		mesh->vao->unbind();
	}
}