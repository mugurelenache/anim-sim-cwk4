// Copyright (c) 2021 Mugurel-Adrian Enache

#define _USE_MATH_DEFINES
#include <cmath>
#include "ObjectFluid.h"
#include "GlobalsWrapper.h"
#include "BoundingSphere.h"
#include <iostream>

#include "VertexBufferObject.h"
#include "IndexBufferObject.h"

#include <omp.h>

/**
 * @brief Constructor
 * @param gw
*/
ObjectFluid::ObjectFluid(GlobalsWrapper* gw) : Object(gw)
{
}

/**
 * @brief Draws the particles and the grid
 * @param mode
 * @param shaderProgramID
*/
void ObjectFluid::draw(GLenum mode, const int& shaderProgramID)
{
	for (auto& particle : particles)
	{
		particle.draw(mode, shaderProgramID);
	}

	grid->draw(shaderProgramID);
}

/**
 * @brief For each particle, check where it is in the grid, then add it to the cell
*/
void ObjectFluid::refreshGridParticles()
{
	// Adding the particles to the cells
	for (size_t i = 0; i < particles.size(); i++)
	{
		// Update the cells
		glm::vec4 pos = glm::vec4(particles[i].transform.position.x, particles[i].transform.position.y, particles[i].transform.position.z, 1.0f);

		int cellID = grid->getCellAtPosition(pos);

		if (cellID >= 0 && cellID < grid->getCellsX() * grid->getCellsY())
		{
			grid->cells[cellID].particles.push_back(i);
		}
	}
}

/**
 * @brief Calculates the particles densities and pressures
 *
 * For each particle, we check if it is in the grid, and if so
 * we will calculate the density using the poly6 kernel
 * as well as the pressure with k(rho - rho_init)
*/
void ObjectFluid::calculateParticlesDensityAndPressure()
{
	// Calculates the densities & pressures
#pragma omp parallel for
	for (size_t i = 0; i < particles.size(); i++)
	{
		float density = 0.0f;

		glm::vec4 pos = glm::vec4(particles[i].transform.position.x, particles[i].transform.position.y, particles[i].transform.position.z, 1.0f);

		int cellID = grid->getCellAtPosition(pos);

		if (cellID == -1 || cellID >= grid->getCellsX() * grid->getCellsY())
		{
			continue;
		}

		std::vector<uint32_t> particlesToQuery = grid->getParticlesInCells(grid->getNeighboringCellsOfCell(cellID));

		// Accumulating the density
#pragma omp parallel for reduction(+:density)
		for (size_t j = 0; j < particlesToQuery.size(); j++)
		{
			density +=
				particles[particlesToQuery[j]].rigidBody->mass
				* kernel(static_cast<KernelType>(KernelType::Poly6), Derivation::None, glm::distance(particles[i].transform.position, particles[particlesToQuery[j]].transform.position), radiusMultiplier * generalParticleRadius);
		}

		// Saving density and pressure
		particles[i].density = density;
		particles[i].pressure = gasConstant * (particles[i].density - particles[i].initialDensity);
	}
}

/**
 * @brief Calculates the Color Field for each Particle
 *
 * If we can apply surface tension, we will calculate the color field
 * similar to the way we calculate densities. However, we're now calculating
 * each particle colorField with all the other existing particles, treating the
 * fluid as a whole.
 *
 * We also determine whether the cells are surface parts
*/
void ObjectFluid::calculateParticlesColorFieldForSurfaceTensionAndSurfaceParts()
{
	if (shouldApplySurfaceTension)
	{

#pragma omp parallel for
		// For each cell, compute the color field
		for (size_t i = 0; i < grid->cells.size(); i++)
		{
			auto& cell = grid->cells[i];
			float colorField = 0.0f;

			// For each particle x each particle
#pragma omp parallel for reduction(+:colorField)
			for (size_t j = 0; j < cell.particles.size(); j++)
			{
				for (size_t k = 0; k < cell.particles.size(); k++)
				{
					float r = glm::distance(particles[cell.particles[j]].transform.position, particles[cell.particles[k]].transform.position);

					if (std::abs(particles[cell.particles[k]].density <= 0.0001f))
					{
						continue;
					}

					colorField +=
						particles[cell.particles[k]].rigidBody->mass
						* (1.0f / particles[cell.particles[k]].density)
						* kernel(KernelType::Poly6, Derivation::None, r, radiusMultiplier * generalParticleRadius);
				}
			}

			// Normalizing the color field
			if (cell.particles.size())
			{
				colorField /= cell.particles.size();
			}

			cell.colorField = colorField;
			cell.isSurface = false;
		}

		// Determine whether the cells are surface parts
#pragma omp parallel for
		for (size_t i = 0; i < grid->cells.size(); i++)
		{
			if (grid->cells[i].colorField <= 0.001f)
			{
				continue;
			}

			std::vector<uint32_t> neighboringCells = grid->getNeighboringCellsOfCell(static_cast<uint32_t>(i));
			for (size_t j = 0; j < neighboringCells.size(); j++)
			{
				if (grid->cells[neighboringCells[j]].colorField == 0.0f)
				{
					grid->cells[i].isSurface = true;
					break;
				}
			}
		}
	}
}

/**
 * @brief Calculating all forces for each particle
 *
 * Calculates internal pressure, surface tension, viscosity, colorfield grad, colorfield laplacian, as well as air resistance.
*/
void ObjectFluid::calculateParticlesForcesAndSimulate(const float& timeStep)
{
#pragma omp parallel for
	for (size_t i = 0; i < particles.size(); i++)
	{
		glm::vec4 forces = glm::vec4(0.0f);
		glm::vec4 internalPressureForce = glm::vec4(0.0f);
		glm::vec4 surfaceTensionForce = glm::vec4(0.0f);
		glm::vec4 viscosityForce = glm::vec4(0.0f);
		glm::vec4 colorFieldGrad = glm::vec4(0.0f);
		glm::vec4 colorFieldLaplacian = glm::vec4(0.0f);

		glm::vec4 pos = glm::vec4(particles[i].transform.position.x, particles[i].transform.position.y, particles[i].transform.position.z, 1.0f);

		int cellID = grid->getCellAtPosition(pos);

		if (cellID == -1 || cellID >= grid->getCellsX() * grid->getCellsY())
		{
			continue;
		}

		std::vector<uint32_t> particlesToQuery = grid->getParticlesInCells(grid->getNeighboringCellsOfCell(cellID));

		// Calculating the forces with respect to the neighboring particles
#pragma omp parallel for reduction(+:internalPressureForce, viscosityForce, colorFieldGrad, colorFieldLaplacian, surfaceTensionForce, forces)
		for (size_t j = 0; j < particlesToQuery.size(); j++)
		{
			float r = glm::distance(particles[i].transform.position, particles[particlesToQuery[j]].transform.position);

			// Checks for small numbers
			if (std::abs(r) <= 0.001f)
			{
				continue;
			}

			if (std::abs(particles[particlesToQuery[j]].density <= 0.0001f))
			{
				continue;
			}

			// Normal calculation
			glm::vec3 ij = glm::normalize(particles[i].transform.position - particles[particlesToQuery[j]].transform.position);
			glm::vec4 relativeDirection = glm::vec4(ij.x, ij.y, ij.z, 1.0f);

			// Forces calculations
			internalPressureForce +=
				-relativeDirection
				* particles[particlesToQuery[j]].rigidBody->mass
				* ((particles[i].pressure + particles[particlesToQuery[j]].pressure) / (2.f * particles[particlesToQuery[j]].density))
				* kernel(static_cast<KernelType>(internalPressureKernelID), Derivation::Gradient, r, radiusMultiplier * generalParticleRadius);

			if (shouldApplyViscosity)
			{
				viscosityForce +=
					particles[particlesToQuery[j]].rigidBody->mass
					* ((particles[particlesToQuery[j]].rigidBody->velocity - particles[i].rigidBody->velocity) / particles[particlesToQuery[j]].density)
					* kernel(KernelType::Viscosity, Derivation::Laplacian, r, radiusMultiplier * generalParticleRadius);
			}

			if (shouldApplySurfaceTension)
			{
				colorFieldGrad +=
					relativeDirection
					* particles[particlesToQuery[j]].rigidBody->mass
					* (1.0f / particles[particlesToQuery[j]].density)
					* kernel(KernelType::Poly6, Derivation::Gradient, r, radiusMultiplier * generalParticleRadius);

				colorFieldLaplacian +=
					particles[particlesToQuery[j]].rigidBody->mass
					* (1.0f / particles[particlesToQuery[j]].density)
					* kernel(KernelType::Poly6, Derivation::Laplacian, r, radiusMultiplier * generalParticleRadius);
			}
		}

		// If we can apply, we gather what we've calculated
		if (shouldApplySurfaceTension)
		{
			float len = std::sqrt(colorFieldGrad.x * colorFieldGrad.x + colorFieldGrad.y * colorFieldGrad.y);
			if (len >= 0.01f)
			{
				surfaceTensionForce = -surfaceTensionCoefficient * colorFieldLaplacian * glm::vec4(colorFieldGrad.x / len, colorFieldGrad.y / len, 0.0f, 0.0f);
			}
		}


		glm::vec4 airResistanceForce = -particles[i].rigidBody->velocity * airResistanceCoefficient;
		forces = internalPressureForce + viscosityCoefficient * viscosityForce + airResistanceForce + surfaceTensionForce;
		particles[i].simulate(timeStep, forces);
	}
}

/**
 * @brief Performs simulation
 * @param timeStep
 * @param externalForces
*/
void ObjectFluid::simulate(const float& timeStep, const glm::vec4& externalForces)
{
	grid->resetCells();

	refreshGridParticles();
	calculateParticlesDensityAndPressure();
	calculateParticlesColorFieldForSurfaceTensionAndSurfaceParts();
	calculateParticlesForcesAndSimulate(timeStep);
}

/**
 * @brief Calculates the number of particles on X
 * @return the calculated number
*/
int ObjectFluid::getParticlesOnX()
{
	if (generalParticleRadius == 0)
	{
		return 0;
	}

	return width / (2.f * generalParticleRadius);
}

/**
 * @brief Calculates the number of particles on Y
 * @return the calculated number
*/
int ObjectFluid::getParticlesOnY()
{
	if (generalParticleRadius == 0)
	{
		return 0;
	}

	return height / (2.f * generalParticleRadius);
}

/**
 * @brief Resets the fluid object
*/
void ObjectFluid::reset()
{
	int xCount = getParticlesOnX();
	int yCount = getParticlesOnY();

	float depth = 0.1f; // 0.1 meters
	float fluidMass = width * height * depth * 1000;

	float particleMass = 0.1f;

	if (xCount && yCount)
	{
		particleMass = fluidMass / (xCount * yCount);
	}

	glm::vec4 center = glm::vec4(transform.position.x, transform.position.y, transform.position.z, 1.0f);
	glm::vec4 leftTop = center + glm::vec4(-width / 2.f + generalParticleRadius, height - generalParticleRadius, 0.0f, 1.0f);

	particles.clear();

	// Positions the particles
	for (int y = 0; y < yCount; y++)
	{
		for (int x = 0; x < xCount; x++)
		{
			ObjectParticle temp = ObjectParticle(
				globals->discMesh,
				globals->discMaterial,
				generalParticleRadius,
				globals);

			temp.transform.setPosition(glm::vec3(leftTop.x + x * 2 * generalParticleRadius, leftTop.y - y * 2 * generalParticleRadius, center.z));
			temp.transform.setScale(glm::vec3(generalParticleRadius, generalParticleRadius, generalParticleRadius));

			temp.rigidBody->mass = particleMass;
			particles.push_back(temp);
		}
	}

	// Calculates the initial densities and pressures
	for (size_t i = 0; i < particles.size(); i++)
	{
		float density = 0.0f;

		for (size_t j = 0; j < particles.size(); j++)
		{
			density +=
				particles[j].rigidBody->mass
				* kernel(static_cast<KernelType>(KernelType::Poly6), Derivation::None, glm::distance(particles[i].transform.position, particles[j].transform.position), radiusMultiplier * generalParticleRadius);
		}

		particles[i].initialDensity = density;
		particles[i].density = density;
		particles[i].pressure = gasConstant * (particles[i].density - particles[i].initialDensity);
	}

	// Positions the grid
	const auto& containerPos = globals->containerObject->transform.position;
	glm::vec4 leftCornerOfContainer = glm::vec4(containerPos.x - (globals->containerObject->width / 2.f), containerPos.y, -0.5f, 1.0f);

	// Generating the grid
	float maxH = std::max(globals->containerObject->transform.position.y + globals->containerObject->height, transform.position.y + height + 2.f * generalParticleRadius);

	float s = radiusMultiplier * generalParticleRadius;
	float w = s * std::ceil(globals->containerObject->width / s);
	float h = s * std::ceil(maxH / s);

	grid.reset(new Grid(leftCornerOfContainer, s, s, w, h, this));
	grid->create();
}

/**
 * @brief Calculates the value for the kernel
 * @param kernelType
 * @param derivation
 * @param r
 * @param h
 * @return result as a float
*/
float ObjectFluid::kernel(const KernelType& kernelType, const Derivation& derivation, const float& r, const float& h)
{
	if (0 <= r && r <= h)
	{
		float h2 = h * h;
		float h3 = h * h * h;
		float h6 = h * h * h * h * h * h;
		float h9 = h * h * h * h * h * h * h * h * h;

		float r2 = r * r;
		float r3 = r * r * r;

		float hr = h - r;
		float hr2 = hr * hr;
		float hr3 = hr * hr * hr;

		float hrsq = h2 - r2;
		float hrsq2 = hrsq * hrsq;
		float hrsq3 = hrsq * hrsq * hrsq;

		switch (kernelType)
		{
		case KernelType::Poly6:
		{
			switch (derivation)
			{
			case Derivation::None:
			{
				return (315.0f / (64.f * (float)M_PI * h9)) * hrsq3;
				break;
			}
			case Derivation::Gradient:
			{
				return -r * (945.f / (32.f * (float)M_PI * h9)) * hrsq2;
				break;
			}
			case Derivation::Laplacian:
			{
				return -(945.0f / (32.0f * (float)M_PI * h9)) * (hrsq * (-6.0f * r2 + 2.0f * h2));
				break;
			}
			}
			break;
		}
		case KernelType::DebrunSpiky:
		{
			switch (derivation)
			{
			case Derivation::None:
			{
				return (15.0f / ((float)M_PI * h6)) * (hr3);
				break;
			}
			case Derivation::Gradient:
			{
				return (-45.0f / ((float)M_PI * h6)) * (hr2);
				break;
			}
			case Derivation::Laplacian:
			{
				return 90.f / ((float)M_PI * h6) * hr * r;
				break;
			}
			}
			break;
		}
		case KernelType::Viscosity:
		{
			switch (derivation)
			{
			case Derivation::None:
			{
				return (15.0f / (2 * (float)M_PI * h3)) * (-r3 / (2.0f * h3) + r2 / h2 + h / (2.0f * r) - 1.0f);
			}
			case Derivation::Gradient:
			{
				return (15.0f / (2 * (float)M_PI * h3)) * ((-3 * r2) / (2.0f * h3) + (2 * r) / h2 + h / 2.0f);
				break;
			}
			case Derivation::Laplacian:
			{
				return (45.0f / ((float)M_PI * h6)) * (hr);
				break;
			}
			}
			break;
		}
		}
	}
	else
	{
		return 0.0f;
	}
}
