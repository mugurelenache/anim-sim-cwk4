// Copyright (c) 2021 Mugurel-Adrian Enache

#pragma once
#include <examples/imgui_impl_glfw.h>
#include <examples/imgui_impl_opengl3.h>
#include "imfilebrowser.h"
#include <memory>
#include <string>
#include "UIScene.h"

/**
 * @brief UIHandler
 *
 * Contains a pointer to a GLFWwindow and a list of UIScene
 *
 * Responsible for the batch rendering of the scenes.
*/
class UIHandler
{
public:
	std::shared_ptr<GLFWwindow*> window;
	std::vector<std::shared_ptr<UIScene>> uiScenes;

public:
	// Default Constructor
	UIHandler();

	// Destructor
	~UIHandler();

	void initWindowAndGL3(std::shared_ptr<GLFWwindow*> window, const std::string& glslVersion);
	void nextFrame();
	void render();
};

