// Copyright (c) 2021 Mugurel-Adrian Enache

#pragma once
#include "Object.h"
#include "ObjectCamera.h"
#include "ObjectLight.h"
#include "ObjectFluid.h"
#include "ObjectContainer.h"
#include "ObjectCollider.h"

#include "BoundingSphere.h"
#include "BoundingBox.h"

#include "Mesh.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <chrono>


/**
 * @brief GlobalsWrapper
 *
 * Due to the forward declaration problems, the global wrappers
 * is a class containing data that could be accessed globally.
*/
class GlobalsWrapper
{
public:
	std::unique_ptr<Mesh> originalMesh;
	std::shared_ptr<Mesh> mesh;

	std::shared_ptr<ObjectCamera> camera;

	std::vector<std::shared_ptr<ObjectLight>> lights;

	std::shared_ptr<ObjectLight> lightObject;

	float mouseDepthValue = 0;
	float mousePosX = 0.f;
	float mousePosY = 0.f;
	bool isMouseInFramebuffer = false;

	int width = 1600;
	int height = 900;

	int renderWidth = 900;
	int renderHeight = 800;

	int selectedRigidBody = -1;
	int hoverSelectedJoint = -1;
	float fps = 0.0f;

	float airResistance = 1.0f;

	std::shared_ptr<ObjectContainer> containerObject;

	std::shared_ptr<ObjectFluid> fluidObject;
	std::shared_ptr<Mesh> discMesh;
	std::shared_ptr<Material> discMaterial;

	std::shared_ptr<Mesh> container;
	std::shared_ptr<Material> containerMaterial;

	bool shouldSimulate = false;

public:
	void generalInit();
};

