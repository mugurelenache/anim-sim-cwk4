// Copyright (c) 2021 Mugurel-Adrian Enache

#pragma once
#include <cstdint>
#include <string>

/**
 * @brief ShaderModule
 *
 * Responsible for compiling and linking multiple shaders.
 * Currently it can do that for vertex/fragment combination.
*/
class ShaderModule
{
public:
	uint32_t id = 0;

public:
	ShaderModule(const std::string& vertexShaderPath, const std::string& fragmentShaderPath);
	void use();
	void unbind();

private:
	void checkCompileShader(const uint32_t& shaderID) const;
	void checkLinkShader(const uint32_t& programID) const;
};

