// Copyright (c) 2021 Mugurel-Adrian Enache

#pragma once
#include <memory>
#include <iostream>

#include <GL/gl3w.h>
#include <GLFW/glfw3.h>

#include <examples/imgui_impl_glfw.h>
#include <examples/imgui_impl_opengl3.h>

#include "imfilebrowser.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <vector>
#include "UIHandler.h"
#include "MainScene.h"
#include "ShaderModule.h"
#include "GlobalsWrapper.h"

#include "Mesh.h"

/**
 * @brief Renderer
 *
 * Takes care of the pure-rendering elements such as GLFW
 * shader programs, framebuffers and GL + initialization.
 *
 */
class Renderer
{
public:
	std::shared_ptr<GLFWwindow*> window;
	std::unique_ptr<UIHandler> userInterface;

private:
	// Pointer to globals wrapper
	std::shared_ptr<GlobalsWrapper> globals;

	// Pointer to color shader program
	std::unique_ptr<ShaderModule> colorShaderProgram;

	// Pointer to grid shader program
	std::unique_ptr<ShaderModule> gridShaderProgram;

	// Ptr to light shader program
	std::unique_ptr<ShaderModule> lightShaderProgram;
	std::unique_ptr<ShaderModule> lightTexShaderProgram;

	// Framebuffer id to keep track of where it has been rendered
	uint32_t renderFramebuffer;

	// Color Texture is the texture which is rendered to in the color pass
	uint32_t colorTexture;

	// Clear color values
	ImVec4 clearColor = ImVec4(0.2f, 0.2f, 0.2f, 1.0f);

	uint32_t savedFrames = 0;

public:
	// Constructor
	Renderer();

	// Destructor
	~Renderer();

	// Initializes the UI
	void initUI();

	// Initializes GLFW & GL
	void generalInit();

	// GL Rendering Loop
	void renderLoop();

	void saveFrame();
};

