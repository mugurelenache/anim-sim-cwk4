#include "ObjectContainer.h"

ObjectContainer::ObjectContainer(std::shared_ptr<Mesh> mesh, std::shared_ptr<Material> material, GlobalsWrapper* gw) : Object(gw)
{
	this->mesh = mesh;
	this->material = material;

	// TODO Handle collider creation
}

ObjectContainer::ObjectContainer(const ObjectContainer& other) : Object(other)
{
	this->mesh = other.mesh;
	this->material = other.material;

	// TODO Handle collider creation
}

void ObjectContainer::draw(GLenum mode, const int& shaderProgramID)
{
	glUniform4fv(glGetUniformLocation(shaderProgramID, "objectMaterial.ambient"), 1, &material->ambient[0]);
	glUniform4fv(glGetUniformLocation(shaderProgramID, "objectMaterial.diffuse"), 1, &material->diffuse[0]);
	glUniform4fv(glGetUniformLocation(shaderProgramID, "objectMaterial.specular"), 1, &material->specular[0]);
	glUniform4fv(glGetUniformLocation(shaderProgramID, "objectMaterial.emission"), 1, &material->emission[0]);
	glUniform1f(glGetUniformLocation(shaderProgramID, "objectMaterial.specularPower"), material->specularPower);
	glUniform1i(glGetUniformLocation(shaderProgramID, "objectMaterial.hasTexture"), material->textureID);
	glUniform4fv(glGetUniformLocation(shaderProgramID, "objectMaterial.emission"), 1, &material->emission[0]);

	glUniformMatrix4fv(glGetUniformLocation(shaderProgramID, "model"), 1, GL_FALSE, (&transform.matrix[0][0]));

	mesh->vao->bind();
	mesh->vao->draw(mode);
	mesh->vao->unbind();
}
