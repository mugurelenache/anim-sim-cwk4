// Copyright (c) 2021 Mugurel-Adrian Enache

#include "Collider3D.h"

/**
 * @brief Creates a collider 3D
 * @param center
 * @param parent
*/
Collider3D::Collider3D(const glm::vec4& center, Object* parent) : Component(parent)
{
	this->center = center;
}
