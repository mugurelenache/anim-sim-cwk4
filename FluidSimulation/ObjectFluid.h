// Copyright (c) 2021 Mugurel-Adrian Enache

#pragma once
#include "Object.h"
#include <vector>
#include "ObjectParticle.h"
#include "Mesh.h"
#include "Grid.h"

/**
 * @brief ObjectFluid class is the wrapper for the particle system
 *
 * It contains a vector to simulated particles
*/
class ObjectFluid : public Object
{
public:
	std::vector<ObjectParticle> particles;

	// in meters
	float generalParticleRadius = 0.15f;
	float width = 1.0f;
	float height = 1.0f;

	int integrationID = 0;
	const char* integrationSchemes[1] = { "Leapfrog" };

	int kernelID = 0;
	const char* kernels[2] = { "Poly6", "Debrun's Spiky" };

	int internalPressureKernelID = 0;
	const char* internalPressureKernels[2] = { "Poly6", "Debrun's Spiky" };

	bool shouldApplyViscosity = false;
	bool shouldApplySurfaceTension = false;

	float gasConstant = 20.f;

	float stepSize = 0.01f;

	float airResistanceCoefficient = 0.0001f;
	float viscosityCoefficient = 50.f;

	float radiusMultiplier = 3.0f;

	float surfaceTensionCoefficient = 10.f;

	std::unique_ptr<Grid> grid;

public:
	ObjectFluid(GlobalsWrapper* gw = nullptr);
	void draw(GLenum mode = GL_TRIANGLES, const int& shaderProgramID = 0);
	void simulate(const float& timeStep, const glm::vec4& externalForces = glm::vec4(0.0f));

	int getParticlesOnX();
	int getParticlesOnY();
	void reset();

private:
	void refreshGridParticles();
	void calculateParticlesDensityAndPressure();
	void calculateParticlesColorFieldForSurfaceTensionAndSurfaceParts();
	void calculateParticlesForcesAndSimulate(const float& timeStep);

public:
	enum class KernelType
	{
		Poly6 = 0,
		DebrunSpiky = 1,
		Viscosity = 2,
	};

	enum class Derivation
	{
		None,
		Gradient,
		Laplacian
	};

	static float kernel(const KernelType& kernelType, const Derivation& derivation, const float& r, const float& h);
};

