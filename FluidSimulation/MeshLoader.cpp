// Copyright (c) 2020 Mugurel-Adrian Enache

#include "MeshLoader.h"
#include <cassert>
#include <sstream>


/**
 * @brief Splits a string via a delimiter, storing the tokens in a vector
 * @param s
 * @param delimiter
 * @return
*/
std::vector<std::string> split(std::string s, std::string delimiter)
{
	size_t posStart = 0;
	size_t posEnd = 0;
	size_t delimLen = delimiter.length();

	std::string token;
	std::vector<std::string> res;

	while ((posEnd = s.find(delimiter, posStart)) != std::string::npos)
	{
		token = s.substr(posStart, posEnd - posStart);
		posStart = posEnd + delimLen;
		res.push_back(token);
	}

	res.push_back(s.substr(posStart));
	return res;
}

/**
 * @brief Loads a mesh given a path
 * @param pathToLoad
*/
void MeshLoader::load(const std::string& pathToLoad)
{
	std::ifstream file;

	file.open(pathToLoad);
	assert(file.is_open() && "MeshLoader could not open file.");

	inVertices.clear();
	indicesVertices.clear();

	// As long as we can read from the file
	while (file.good())
	{
		// Start getting lines
		std::string temp;
		std::getline(file, temp);

		// Loading them into a stringstream
		std::stringstream line(temp);

		// Tokenizing
		std::string token;

		// Read first string
		line >> token;

		// Check for first parameter
		// Is it a vertex
		if (token == "v")
		{
			// We only read at most 4 coords per line
			uint32_t vertexCoordCounter = 0;
			while (line >> token)
			{
				vertexCoordCounter++;
				if (vertexCoordCounter == 4)
				{
					// We have finished the read from a vertex (which must have at most 4 coords)
					break;
				}

				float vertexCoord = std::stof(token);
				inVertices.push_back(vertexCoord);
			}
		}
		// Is it a face
		else if (token == "f")
		{
			while (line >> token)
			{
				size_t pos = 0;
				std::string delimiter = "/";

				auto strvec = split(token, delimiter);
				for (uint32_t count = 0; count < strvec.size(); count++)
				{
					std::string elem = strvec[count];
					if (elem.length() > 0)
					{
						int val = std::stoi(elem);
						if (count == 0)
						{
							int index = val > 0 ?
								val - 1 :
								val + static_cast<int>(inVertices.size()) / 3;

							indicesVertices.push_back(index);
						}
						else if (count == 1)
						{
							int index = val > 0 ?
								val - 1 :
								val + static_cast<int>(inTexCoords.size()) / 2;
							indicesTexCoords.push_back(index);
						}
						else if (count == 2)
						{
							int index = val > 0 ?
								val - 1 :
								val + static_cast<int>(inNormals.size()) / 3;

							indicesNormals.push_back(index);
						}
					}
				}
			}
		}
		else if (token == "vn")
		{
			// We only read at most 4 coords per line
			uint32_t vertexCoordCounter = 0;
			while (line >> token)
			{
				vertexCoordCounter++;
				if (vertexCoordCounter == 4)
				{
					// We have finished the read from a vertex (which must have at most 4 coords)
					break;
				}

				float normalCoord = std::stof(token);
				inNormals.push_back(normalCoord);
			}
		}
		else if (token == "vt")
		{
			// We only read at most 3 coords per line
			uint32_t vertexCoordCounter = 0;
			while (line >> token)
			{
				vertexCoordCounter++;
				if (vertexCoordCounter == 3)
				{
					// We have finished the read from a vertex (which must have at most 3 coords)
					break;
				}

				float vertexTextureCoord = std::stof(token);
				inTexCoords.push_back(vertexTextureCoord);
			}
		}
	}

	file.close();
}

/**
 * @brief Saves a mesh into a file
 * @param pathToSave
 * @param mesh
*/
void MeshLoader::save(const std::string& pathToSave, std::shared_ptr<Mesh> mesh)
{
	std::ofstream file;

	file.open(pathToSave + ".obj");
	assert(file.is_open() && "MeshLoader could not open file.");

	// Generating a nice header
	file << "# MESHLOADER OBJ EXPORTER" << std::endl;
	file << "# " << mesh->vertexBuffer->elements.size() << " VERTICES" << std::endl;
	file << "# " << mesh->indexBuffer->elements.size() << " INDICES" << std::endl;
	file << "# " << mesh->indexBuffer->elements.size() / 3 << " FACES" << std::endl << std::endl;

	// Generating a bit of metadata about inVertices
	file << "# VERTICES" << std::endl;
	for (uint32_t i = 0; i < mesh->vertexBuffer->elements.size(); i++)
	{
		file << "v ";

		file << mesh->vertexBuffer->elements[i].position.x << " ";
		file << mesh->vertexBuffer->elements[i].position.y << " ";
		file << mesh->vertexBuffer->elements[i].position.z;

		file << std::endl;
	}

	file << "# NORMALS" << std::endl;
	for (uint32_t i = 0; i < mesh->vertexBuffer->elements.size(); i++)
	{
		file << "vn ";

		file << mesh->vertexBuffer->elements[i].normal.x << " ";
		file << mesh->vertexBuffer->elements[i].normal.y << " ";
		file << mesh->vertexBuffer->elements[i].normal.z;

		file << std::endl;
	}

	file << "# TEXCOORDS" << std::endl;
	for (uint32_t i = 0; i < mesh->vertexBuffer->elements.size(); i++)
	{
		file << "vt ";

		file << mesh->vertexBuffer->elements[i].uv.x << " ";
		file << mesh->vertexBuffer->elements[i].uv.y;

		file << std::endl;
	}

	// Generating a bit of metadata about faces
	file << std::endl << "# FACES" << std::endl;
	for (uint32_t i = 0; i < mesh->indexBuffer->elements.size(); i += 3)
	{
		file << "f ";

		for (uint32_t j = 0; j < 3; j++)
		{
			uint32_t pos = i + j;
			auto posID = mesh->indexBuffer->elements[pos] + 1;
			file << posID << "/" << posID << "/" << posID << " ";
		}

		file << std::endl;
	}

	file.close();
}

/**
 * @brief Engine-specific, generates the VBO and IBOs
*/
void MeshLoader::generateBuffers()
{
	for (uint32_t i = 0; i < indicesVertices.size(); i++)
	{
		Vertex temp;
		temp.color = glm::vec4(1.0f);

		if (i < indicesVertices.size())
		{
			size_t vertexID = indicesVertices[i];
			auto maxID = 3 * vertexID + 2;

			if (maxID < inVertices.size())
			{
				temp.position = glm::vec4(inVertices[3 * vertexID], inVertices[3 * vertexID + 1], inVertices[3 * vertexID + 2], 1.0f);
			}
		}

		if (i < indicesNormals.size())
		{
			size_t normalID = indicesNormals[i];
			auto maxID = 3 * normalID + 2;

			if (maxID < inNormals.size())
			{
				temp.normal = glm::vec4(inNormals[3 * normalID], inNormals[3 * normalID + 1], inNormals[3 * normalID + 2], 1.0f);
			}
		}

		if (i < indicesTexCoords.size())
		{
			size_t texCoordID = indicesTexCoords[i];
			auto maxID = 2 * texCoordID + 1;

			if (maxID < inTexCoords.size())
			{
				temp.uv = glm::vec2(inTexCoords[2 * texCoordID], inTexCoords[2 * texCoordID + 1]);
			}
		}

		// Check whether the element exist
		// and if not, add it to the array
		//
		// complete the index buffer
		int val = elementExists(vertexBuffer, temp);

		if (val == -1)
		{
			vertexBuffer.push_back(temp);
			indexBuffer.push_back(vertexBuffer.size() - 1);
		}
		else
		{
			indexBuffer.push_back(val);
		}
	}
}

/**
 * @brief Checks if an element exist within a vertex vector
 * @param arrayToCheck
 * @param check
 * @return
*/
int MeshLoader::elementExists(const std::vector<Vertex>& arrayToCheck, const Vertex& check) const
{
	for (uint32_t i = 0; i < arrayToCheck.size(); i++)
	{
		if (arrayToCheck[i] == check)
		{
			return static_cast<int>(i);
		}
	}

	return -1;
}

/**
 * @brief Clears all the vectors
*/
void MeshLoader::clear()
{
	vertices.clear();
	normals.clear();
	texCoords.clear();
	colors.clear();
	vertexBuffer.clear();
	indexBuffer.clear();
	inVertices.clear();
	inNormals.clear();
	inTexCoords.clear();
	indicesVertices.clear();
	indicesTexCoords.clear();
	indicesNormals.clear();
}
