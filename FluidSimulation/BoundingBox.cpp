// Copyright (c) 2021 Mugurel-Adrian Enache

#include "BoundingBox.h"

/**
 * @brief Creates a bounding box given center, w,h,d
 * @param center
 * @param width
 * @param height
 * @param depth
 * @param parent
*/
BoundingBox::BoundingBox(const glm::vec4& center, const float& width, const float& height, const float& depth, Object* parent) : Collider3D(center, parent)
{
	this->width = width;
	this->height = height;
	this->depth = depth;

	float eps = 0.01f;

	minX = center.x - width / 2.f - eps;
	maxX = center.x + width / 2.f + eps;

	minY = center.y - height / 2.f - eps;
	maxY = center.y + height / 2.f + eps;

	minZ = center.z - depth / 2.f - eps;
	maxZ = center.z + depth / 2.f + eps;
}

/**
 * @brief Checks whether the bounding box contains a vertex position
 * @param position
 * @return bool given by condition
*/
bool BoundingBox::isInside(const glm::vec4& position) const
{
	return (position.x >= minX && position.x <= maxX) && (position.y >= minY && position.y <= maxY) && (position.z >= minZ && position.z <= maxZ);
}

bool BoundingBox::isClose(const glm::vec4& position) const
{
	return (position.x >= minX - eps && position.x <= maxX + eps) && (position.y >= minY - eps && position.y <= maxY + eps) && (position.z >= minZ - eps && position.z <= maxZ + eps);
}

/**
 * @brief Updates the corners via center
*/
void BoundingBox::updateViaCenter()
{
	minX = center.x - width / 2.f - eps;
	maxX = center.x + width / 2.f + eps;

	minY = center.y - height / 2.f - eps;
	maxY = center.y + height / 2.f + eps;

	minZ = center.z - depth / 2.f - eps;
	maxZ = center.z + depth / 2.f + eps;
}
