// Copyright (c) 2021 Mugurel-Adrian Enache

#include "LightProperties.h"
#include "Object.h"

/**
 * @brief Constructor of light properties (similar to material)
 * @param diffuse
 * @param specular
 * @param ambient
 * @param specularPower
 * @param object
*/
LightProperties::LightProperties(const glm::vec4& diffuse, const glm::vec4& specular, const glm::vec4 ambient, float specularPower, Object* object) : Component(object)
{
	this->diffuse = diffuse;
	this->specular = specular;
	this->ambient = ambient;
	this->specularPower = specularPower;
}