// Copyright (c) 2021 Mugurel-Adrian Enache

#pragma once
#include <vector>
#include <GL/gl3w.h>

/**
 * @brief IndexBufferObject
 *
 * Responsible for holding index data.
 * @tparam T
*/
template <class T>
class IndexBufferObject
{
public:
	uint32_t id = 0;
	std::vector<T> elements;

public:
	IndexBufferObject<T>(uint32_t size, void* data);
	IndexBufferObject<T>(const std::vector<T>& data);
	IndexBufferObject<T>(const IndexBufferObject& other);
	IndexBufferObject<T>(IndexBufferObject&& other) noexcept;
	void generate();
	void load();
	void bind();
	void unbind();
	void draw(GLenum mode = GL_TRIANGLES);
	T& operator[](const uint32_t& i);
};

template<class T>
inline IndexBufferObject<T>::IndexBufferObject(uint32_t size, void* data)
{
	for (uint32_t i = 0; i < size; i++)
	{
		elements.push_back(static_cast<T*>(data)[i]);
	}
}

template<class T>
inline IndexBufferObject<T>::IndexBufferObject(const std::vector<T>& data)
{
	for (size_t i = 0; i < data.size(); i++)
	{
		elements.push_back(data[i]);
	}
}

template<class T>
inline IndexBufferObject<T>::IndexBufferObject(const IndexBufferObject& other)
{
	this->id = other.id;
	this->elements = other.elements;
}

template<class T>
inline IndexBufferObject<T>::IndexBufferObject(IndexBufferObject&& other) noexcept
{
	this->id = other.id;
	this->elements = other.elements;
}

template<class T>
inline void IndexBufferObject<T>::generate()
{
	glGenBuffers(1, &id);
}

template<class T>
inline void IndexBufferObject<T>::load()
{
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, elements.size() * sizeof(T), elements.data(), GL_STATIC_DRAW);
}

template<class T>
inline void IndexBufferObject<T>::bind()
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id);
}

template<class T>
inline void IndexBufferObject<T>::unbind()
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

template<class T>
inline void IndexBufferObject<T>::draw(GLenum mode)
{
	glDrawElements(mode, static_cast<GLsizei>(elements.size()), GL_UNSIGNED_INT, 0);
}

template<class T>
inline T& IndexBufferObject<T>::operator[](const uint32_t& i)
{
	return elements[i];
}
