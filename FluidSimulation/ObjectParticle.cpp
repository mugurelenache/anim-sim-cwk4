#include "ObjectParticle.h"

ObjectParticle::ObjectParticle(std::shared_ptr<Mesh> mesh, std::shared_ptr<Material> material, const float& radius, GlobalsWrapper* gw) : Object(gw)
{
	this->mesh = mesh;
	this->material = material;

	this->rigidBody = std::make_unique<RigidBody>();
	this->rigidBody->parent = this;

	glm::vec4 pos = glm::vec4(transform.position.x, transform.position.y, transform.position.z, 1.0f);
	this->collider = std::make_unique<BoundingSphere>(BoundingSphere(pos, radius, this));
	this->collider->parent = this;
}

ObjectParticle::ObjectParticle(const ObjectParticle& other) : Object(other)
{
	this->mesh = other.mesh;
	this->material = other.material;

	RigidBody otherRB = *(other.rigidBody.get());

	this->rigidBody = std::make_unique<RigidBody>(otherRB);
	this->rigidBody->parent = this;

	glm::vec4 pos = glm::vec4(other.transform.position.x, other.transform.position.y, other.transform.position.z, 1.0f);
	this->collider = std::make_unique<BoundingSphere>(BoundingSphere(pos, other.collider->radius, this));
	this->collider->parent = this;

	this->pressure = other.pressure;
	this->density = other.density;
	this->initialDensity = other.initialDensity;
}

void ObjectParticle::draw(GLenum mode, const int& shaderProgramID)
{
	glUniform4fv(glGetUniformLocation(shaderProgramID, "objectMaterial.ambient"), 1, &material->ambient[0]);
	glUniform4fv(glGetUniformLocation(shaderProgramID, "objectMaterial.diffuse"), 1, &material->diffuse[0]);
	glUniform4fv(glGetUniformLocation(shaderProgramID, "objectMaterial.specular"), 1, &material->specular[0]);
	glUniform4fv(glGetUniformLocation(shaderProgramID, "objectMaterial.emission"), 1, &material->emission[0]);
	glUniform1f(glGetUniformLocation(shaderProgramID, "objectMaterial.specularPower"), material->specularPower);
	glUniform1i(glGetUniformLocation(shaderProgramID, "objectMaterial.hasTexture"), material->textureID);
	glUniform4fv(glGetUniformLocation(shaderProgramID, "objectMaterial.emission"), 1, &material->emission[0]);

	glUniformMatrix4fv(glGetUniformLocation(shaderProgramID, "model"), 1, GL_FALSE, (&transform.matrix[0][0]));

	mesh->vao->bind();
	mesh->vao->draw(mode);
	mesh->vao->unbind();
}

void ObjectParticle::simulate(const float& timeStep, const glm::vec4& externalForce)
{
	rigidBody->simulate(timeStep, externalForce);
}

/**
 * @brief Updates the center of the collider
*/
void ObjectParticle::updateInternalPositions()
{
	this->collider->center = glm::vec4(transform.position.x, transform.position.y, transform.position.z, 1.0f);
}
