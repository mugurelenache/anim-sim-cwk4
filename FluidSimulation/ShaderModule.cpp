// Copyright (c) 2021 Mugurel-Adrian Enache

#include "ShaderModule.h"
#include <fstream>
#include <sstream>
#include <cassert>
#include <iostream>
#include <GL\gl3w.h>

/**
 * @brief Constructor of a shader module, compiles and links shaders
 *
 * @param vertexShaderPath
 * @param fragmentShaderPath
*/
ShaderModule::ShaderModule(const std::string& vertexShaderPath, const std::string& fragmentShaderPath)
{
	std::string vertCode;
	std::string fragCode;

	std::ifstream vertShaderFile;
	std::ifstream fragShaderFile;

	vertShaderFile.open(vertexShaderPath);
	fragShaderFile.open(fragmentShaderPath);

	assert(vertShaderFile.is_open() && ("Error: Could not open vertex shader"));
	assert(fragShaderFile.is_open() && ("Error: Could not open fragment shader"));

	std::stringstream vertSS;
	std::stringstream fragSS;

	vertSS << vertShaderFile.rdbuf();
	fragSS << fragShaderFile.rdbuf();

	auto vertStringCode = vertSS.str();
	auto fragStringCode = fragSS.str();

	auto vertSCode = vertStringCode.c_str();
	auto fragSCode = fragStringCode.c_str();

	uint32_t vertShader;
	uint32_t fragShader;

	vertShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertShader, 1, &vertSCode, NULL);
	glCompileShader(vertShader);
	checkCompileShader(vertShader);

	fragShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragShader, 1, &fragSCode, NULL);
	glCompileShader(fragShader);
	checkCompileShader(fragShader);

	id = glCreateProgram();
	glAttachShader(id, vertShader);
	glAttachShader(id, fragShader);
	glLinkProgram(id);
	checkLinkShader(id);

	glDeleteShader(vertShader);
	glDeleteShader(fragShader);
}

/**
 * @brief Uses a shader
*/
void ShaderModule::use()
{
	assert(id && "Shader Module ID == 0");
	glUseProgram(id);
}

/**
 * @brief Stops using an used shader
*/
void ShaderModule::unbind()
{
	glUseProgram(0);
}

/**
 * @brief Checks if the shader has been compiled
 * @param shaderID
*/
void ShaderModule::checkCompileShader(const uint32_t& shaderID) const
{
	int success;
	char info[512];

	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &success);

	if (!success)
	{
		glGetShaderInfoLog(shaderID, 512, NULL, info);
		std::cerr << "Error: Shader " << shaderID << " was not successfully complied." << std::endl;
		std::cerr << "Log: " << info << std::endl;
		std::exit(-1);
	}
}

/**
 * @brief Checks if the shader program has been linked
 * @param programID
*/
void ShaderModule::checkLinkShader(const uint32_t& programID) const
{
	int success;
	char info[512];

	glGetShaderiv(programID, GL_LINK_STATUS, &success);

	if (!success)
	{
		glGetShaderInfoLog(programID, 512, NULL, info);
		std::cerr << "Error: Program " << programID << " was not successfully linked." << std::endl;
		std::cerr << "Log: " << info << std::endl;
		std::exit(-1);
	}
}