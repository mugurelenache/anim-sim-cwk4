// Copyright (c) 2021 Mugurel-Adrian Enache

#pragma once
#include "Collider3D.h"
#include <glm/glm.hpp>
#include <vector>
#include "Vertex.h"

/**
 * @brief BoundingSphere
 *
 * Usually kept by a mesh.
*/
class BoundingSphere : public Collider3D
{
public:
	float radius = 0.0f;

public:
	BoundingSphere(const glm::vec4& center = glm::vec4(0.0f), const float& radius = 0.0f, Object* parent = nullptr);
	virtual bool isInside(const glm::vec4& position) const;
	virtual bool isClose(const glm::vec4& position) const;
	virtual void updateViaCenter();
};

