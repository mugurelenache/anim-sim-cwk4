// Copyright (c) 2021 Mugurel-Adrian Enache

#include "Material.h"
#include "Object.h"

/**
 * @brief Constructor that creates a material
 * @param ambient
 * @param diffuse
 * @param specular
 * @param emission
 * @param specularPower
 * @param textureID
 * @param object
*/
Material::Material(const glm::vec4& ambient, const glm::vec4& diffuse, const glm::vec4& specular, const glm::vec4& emission, const float& specularPower, const uint32_t& textureID, Object* object) : Component(object)
{
	this->ambient = ambient;
	this->diffuse = diffuse;
	this->specular = specular;
	this->emission = emission;
	this->specularPower = specularPower;
	this->textureID = textureID;
}

/**
 * @brief Copy constructor for a material
 * @param object
*/
Material::Material(Object* object) : Component(object)
{
	ambient = glm::vec4(0.2f, 0.2f, 0.2f, 1.0f);
	diffuse = glm::vec4(1.f, 1.0f, 1.0f, 1.0f);
	specular = glm::vec4(0.5f, 0.5f, 0.5f, 1.0f);
	emission = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	specularPower = 16.f;
	textureID = 0;
}
