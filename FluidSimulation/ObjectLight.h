// Copyright (c) 2021 Mugurel-Adrian Enache

#pragma once
#include "Object.h"
#include "LightProperties.h"

/**
 * @brief An object representing a light
*/
class ObjectLight : public Object
{
public:
	LightProperties properties;

public:
	ObjectLight(LightProperties* lightProperties = nullptr);
	void draw(const int& shaderProgramID);
};