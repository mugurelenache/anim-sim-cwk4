// Copyright (c) 2021 Mugurel-Adrian Enache

#include "ObjectCamera.h"

/**
 * @brief Creates a camera that includes near/far planes and a projection
 * @param nearPlane
 * @param farPlane
 * @param projection
*/
ObjectCamera::ObjectCamera(const float& nearPlane, const float& farPlane, const glm::mat4& projection) : Object()
{
	this->nearPlane = nearPlane;
	this->farPlane = farPlane;
	this->projection = projection;
}