// Copyright (c) 2021 Mugurel-Adrian Enache

#include "BoundingSphere.h"
#include <iostream>

/**
 * @brief Creates a bounding-sphere given center & radius
 * @param center
 * @param radius
 * @param parent
*/
BoundingSphere::BoundingSphere(const glm::vec4& center, const float& radius, Object* parent) : Collider3D(center, parent)
{
	this->radius = radius;
}

/**
 * @brief Checks whether the point
 * @param position
 * @return
*/
bool BoundingSphere::isInside(const glm::vec4& position) const
{
	return glm::distance(center, position) <= radius + eps;
}

bool BoundingSphere::isClose(const glm::vec4& position) const
{
	return glm::distance(center, position) <= radius + 1.1f * eps;
}

void BoundingSphere::updateViaCenter()
{
	// Does nothing
}
