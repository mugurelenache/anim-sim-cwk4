// Copyright (c) 2021 Mugurel-Adrian Enache

#include "UIScene.h"

/**
* Constructor UIScene(uint32_t, uint32_t)
*
* Creates a scene and stores the width, height information
*/
UIScene::UIScene(uint32_t width, uint32_t height)
{
	this->width = width;
	this->height = height;
}