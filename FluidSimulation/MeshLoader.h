// Copyright (c) 2021 Mugurel-Adrian Enache

#pragma once
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include "Mesh.h"

/**
 * @brief MeshLoader
 *
 * Used to save/load OBJ Wavefront files.
*/
class MeshLoader
{
public:
	std::vector<float> vertices;
	std::vector<float> normals;
	std::vector<float> texCoords;
	std::vector<float> colors;

	// Specific to renderer
	std::vector<Vertex> vertexBuffer;
	std::vector<uint32_t> indexBuffer;

private:
	// 3 values are equivalent to one vertex
	std::vector<float> inVertices;
	// 3 values are equivalent to one normal
	std::vector<float> inNormals;
	// 2 values are equivalent to one tex-coord uv
	std::vector<float> inTexCoords;

	// 3 values are equivalent to one face
	std::vector<uint32_t> indicesVertices;
	std::vector<uint32_t> indicesTexCoords;
	std::vector<uint32_t> indicesNormals;

public:
	// Default Constructor
	MeshLoader() = default;

	void load(const std::string& pathToLoad);
	void save(const std::string& pathToSave, std::shared_ptr<Mesh> mesh);

	// Specific to renderer
	void generateBuffers();
	int elementExists(const std::vector<Vertex>& arrayToCheck, const Vertex& check) const;

	void clear();
};

