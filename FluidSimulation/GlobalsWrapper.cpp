// Copyright (c) 2021 Mugurel-Adrian Enache

#include "GlobalsWrapper.h"
#include "MeshLoader.h"

/**
 * @brief General initialization of some global variables
*/
void GlobalsWrapper::generalInit()
{
	// Initializes camera
	camera = std::make_shared<ObjectCamera>();
	camera->transform.setPosition(glm::vec3(0.f, 0.f, 15.f));

	lightObject = std::make_shared<ObjectLight>(nullptr);
	lightObject->transform.setPosition(glm::vec3(0.0f, 150.0f, 100.f));

	// Loading the disc
	{
		MeshLoader ml;

		ml.load("Models/unit_disc.obj");
		ml.generateBuffers();

		auto vbo = VertexBufferObject<Vertex>(ml.vertexBuffer);
		auto ibo = IndexBufferObject<uint32_t>(ml.indexBuffer);

		discMesh = std::make_shared<Mesh>(
			std::make_shared<VertexBufferObject<Vertex>>(vbo),
			std::make_shared<IndexBufferObject<uint32_t>>(ibo)
			);

		discMesh->generateVAO(GL_DYNAMIC_DRAW);
	}

	// Loading the container
	{
		MeshLoader ml;

		ml.load("Models/container3d_faceless.obj");
		ml.generateBuffers();

		auto vbo = VertexBufferObject<Vertex>(ml.vertexBuffer);
		auto ibo = IndexBufferObject<uint32_t>(ml.indexBuffer);

		container = std::make_shared<Mesh>(
			std::make_shared<VertexBufferObject<Vertex>>(vbo),
			std::make_shared<IndexBufferObject<uint32_t>>(ibo)
			);

		container->generateVAO(GL_DYNAMIC_DRAW);
	}

	containerMaterial = std::make_shared<Material>(nullptr);
	containerMaterial->diffuse = glm::vec4(0.8f, 0.8f, 0.8f, 0.4f);

	discMaterial = std::make_shared<Material>(nullptr);
	discMaterial->diffuse = glm::vec4(0.6f, 0.1f, 0.1f, 1.0f);

	containerObject = std::make_shared<ObjectContainer>(container, containerMaterial, this);
	containerObject->height = 5.f;
	containerObject->width = 5.f;
	containerObject->transform.setScale(glm::vec3(containerObject->width, containerObject->height, 1.0f));

	fluidObject = std::make_shared<ObjectFluid>(this);
	fluidObject->width = 2.f;
	fluidObject->height = 3.f;
	fluidObject->transform.setPosition(glm::vec3(0.f, 4.f, 0.f));
	fluidObject->reset();



}