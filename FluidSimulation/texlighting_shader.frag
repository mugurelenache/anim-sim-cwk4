// Copyright (c) 2020 Mugurel-Adrian Enache

#version 450

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 normal;
layout(location = 2) in vec4 color;
layout(location = 3) in vec2 uv;

layout(location = 0) out vec4 colorOut;

uniform vec4 lightPos;
uniform vec4 cameraPos;

// TODO Add sampler2d

struct Material
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 emission;
	float specularPower;
	sampler2D diffuseMap;
	int hasTexture;
};

struct LightProperties
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float specularPower;
};

uniform Material objectMaterial;
uniform LightProperties lightProperties;

vec4 BlinnPhong()
{
	vec4 norm = normalize(normal);
	vec4 lightDir = normalize(lightPos - position);
	vec4 cameraDir = normalize(cameraPos - position);

	vec4 halfVector = normalize(lightPos + cameraDir);

	// Ambient
	vec4 ambientC = objectMaterial.ambient * lightProperties.ambient;

	// Diffuse
	float diffuseIntensity = max(dot(norm, lightDir), 0.0);
	vec4 diffuseC = diffuseIntensity * objectMaterial.diffuse * lightProperties.diffuse;

	float specularIntensity = pow(max(dot(norm, halfVector), 0.0), objectMaterial.specularPower);
	vec4 specularC = specularIntensity * objectMaterial.specular * lightProperties.specular * lightProperties.diffuse;

	vec4 result = ambientC + diffuseC + specularC + objectMaterial.emission;

	if(objectMaterial.hasTexture != 0)
	{
		result = ambientC + diffuseC * texture(objectMaterial.diffuseMap, uv).rgba + specularC + objectMaterial.emission;
	}

	result.w = objectMaterial.diffuse.w;

	return result;
}


void main()
{	
	vec4 color4 = BlinnPhong();
	colorOut = color4;
}