// Copyright (c) 2021 Mugurel-Adrian Enache

#pragma once
#include <GL/gl3w.h>

/**
 * @brief VertexAttribute
 *
 * Responsible for holding data about a vertex attribute.
 * Can be asynchronously turned on during a vertex buffer creation.
*/
class VertexAttribute
{
public:
	GLuint index = 0;
	GLint size = 0;
	GLenum type = 0;
	GLboolean normalized = false;
	GLsizei stride = 0;
	GLvoid* offsetPtr = nullptr;

public:
	// Constructor
	VertexAttribute(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, GLvoid* offsetPtr);
	void activate();
};

