// Copyright (c) 2020 Mugurel-Adrian Enache

#pragma once
#include "ObjectIKMesh.h"
#include <vector>
#include <string>
#include <Eigen/Dense>

class GlobalsWrapper;
class IKWrapper : public std::enable_shared_from_this<IKWrapper>
{
public:
	std::shared_ptr<Mesh> defaultMesh;

	BVHLoader* bvhLoader;

	GlobalsWrapper* globals;


	std::vector<ObjectIKMesh> hierarchy;
	std::vector<ObjectIKMesh*> directAccessOriginalHierarchy;

	float frameTime = 0.0f;
	uint32_t totalFrames = 0;
	std::vector<std::vector<float>> frames;

	uint32_t lastID = 0;
	uint32_t currentFrame = 0;
	uint32_t channelCount = 0;

	bool ignoreRootPositions = false;

	// Used to simulate the joints movements for the
	// IK Transformations
	std::vector<ObjectIKMesh> simulatedHierarchy;
	std::vector<ObjectIKMesh*> directAccessSimulatedHierarchy;

	// Current Frame Data
	// An array containing all the rotations, namely thetaStart
	std::vector<float> thetaStart;
	float deltaThetaRot = 0.001f;

	// UI Display stuff
	std::vector<ObjectIKMesh> uiHierarchy;
	std::vector<ObjectIKMesh*> directAccessUIHierarchy;

public:
	IKWrapper(std::shared_ptr<Mesh> mesh, BVHLoader& bvhLoader, GlobalsWrapper* globals = nullptr);
	~IKWrapper();


	// Adds a child to the original hierarchy
	uint32_t addChild(ObjectIKMesh child);

	// Creates a hierarchy given a vector of joint data
	void createHierarchy(std::vector<BVHLoader::Joint> jointsData);

	// Draws the whole bvh
	void draw(GLenum mode = GL_TRIANGLES, int shaderProgramID = 0);

	// Plays a frame on a given base recursive hierarchy
	void playFrame(uint32_t frameID, std::vector<ObjectIKMesh>& baseHierarchy);

	// Plays a frame on the original hierarchy and propagates the changes onto the others
	void playFrame(uint32_t frameID);

	// Plays the initial position
	void playInitialPos();


	// Gets a joint given an id and a direct access hierarchy
	ObjectIKMesh* getJoint(uint32_t id, std::vector<ObjectIKMesh*>& daHierarchy);

	// Initializes simulated hierarchy
	void initializeSimulatedHierarchy();

	// Initializes UI hierarchy
	void initializeUIHierarchy();

	// Populates a direct hierarchy vector given a base recursive hierarchy
	void populateDirectAccessHierarchy(std::vector<ObjectIKMesh>& baseHierarchy, std::vector<ObjectIKMesh*>& daHierarchyToPopulate);

	// Initializes theta start
	void initializeThetaStart();

	// Updates the inner hierarchy frame data given theta start
	void updateHierarchyFrameData(std::vector<ObjectIKMesh*>& daHierarchy);

	// Sorts a direct access hierarchy in ascending order
	void sortHierarchyByID(std::vector<ObjectIKMesh*>& baseHierarchy);

	// Computes the ik given ui joints ids
	void computeIK(const std::vector<uint32_t>& jointIDs, bool isDamped, float lambda, const std::vector<uint32_t>& controlJointIDs);

	// Resets direct access hierarchy to other direct-access hierarchy
	void resetDATo(std::vector<ObjectIKMesh*>& target, std::vector<ObjectIKMesh*>& other);

	// Resets direct joint to other joint
	void resetJointTo(ObjectIKMesh* target, ObjectIKMesh* other);

	// Pretty print for eigen matrix
	void printMatrixXf(const Eigen::MatrixXf& mat);

	// Generates the gain vector, for the control points
	std::vector<float> generateGainVector(const std::vector<uint32_t>& controlJointsIDs);
};